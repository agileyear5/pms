package pms.test;


import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import ie.ait.eng.sw.msc.agile.dao.PatientsDAO;
import ie.ait.eng.sw.msc.agile.util.Patients;

import junit.framework.Assert;

public class ViewPatientAccountTest {
	private static PatientsDAO patientsDAO;
	private Patients patients ;
	private int testID =1;

	@BeforeClass
	public static void setup() throws Exception {
		patientsDAO = new PatientsDAO();
	}

	@Test
	public void testfindPatient0() throws ClassNotFoundException {

		Patients patients = patientsDAO .findPatient(testID);
		Assert.assertEquals(testID, patients.getId());
		Assert.assertEquals("M",patients.getGender());
		
		return;
	}
	
	@Test
	public void testfindPatient1() throws ClassNotFoundException {

		Patients patients = patientsDAO .findPatient(testID);
		Assert.assertEquals(testID, patients.getId());
		Assert.assertEquals("Woodville, Athlone",patients.getAddress());
		
		return;
	}
	
	@Test
	public void testfindPatient2() throws ClassNotFoundException {

		Patients patients = patientsDAO .findPatient(testID);
		Assert.assertEquals(testID, patients.getId());
		Assert.assertEquals("0876298274",patients.getTelephone());
		
		return;
	}
	@Test
	public void testfindPatient3() throws ClassNotFoundException {

		Patients patients = patientsDAO .findPatient(testID);
		Assert.assertEquals(testID, patients.getId());
		Assert.assertEquals("xu.luo@student.ait.ie",patients.getEmail());
		
		return;
	}
	@Test
	public void testfindPatient4() throws ClassNotFoundException {

		Patients patients = patientsDAO .findPatient(testID);
		Assert.assertEquals(testID, patients.getId());
		Assert.assertEquals("Ossama",patients.getFirstName());
		
		return;
	}  
	@Test
	public void testfindPatient5() throws ClassNotFoundException {

		Patients patients = patientsDAO .findPatient(testID);
		Assert.assertEquals(testID, patients.getId());
		Assert.assertEquals("Alghamdi",patients.getSecondName());
		
		return;
	}   
	

}