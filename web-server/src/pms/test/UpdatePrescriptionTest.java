package pms.test;

import static org.junit.Assert.*;

import org.junit.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import junit.framework.TestCase;

import pms.servlets.gui.PharmacistServlet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class UpdatePrescriptionTest extends Mockito {



	@Test
	public void testDoPostHttpServletRequestHttpServletResponse() throws Exception {

		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);

		when(request.getParameter("pid")).thenReturn("3");
		when(request.getParameter("prescription")).thenReturn("PainKillers");
		when(request.getParameter("allergies")).thenReturn("take 2 a day");
		when(request.getParameter("advice_info")).thenReturn("drink water");
		StringWriter stringWriter = new StringWriter();
		PrintWriter writer = new PrintWriter(stringWriter);
		when(response.getWriter()).thenReturn(writer);

		new PharmacistServlet().doPost(request, response);

		verify(request, atLeast(1)).getParameter("pid");
		verify(request, atLeast(1)).getParameter("prescription");
		verify(request, atLeast(1)).getParameter("allergies");
		verify(request, atLeast(1)).getParameter("advice_info");
	
		writer.flush();
		assertTrue(stringWriter.toString().contains("Update prescription information successfully!"));


	}

}
