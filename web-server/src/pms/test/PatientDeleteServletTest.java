package pms.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;
import pms.servlets.gui.PatientDeleteServlet;

public class PatientDeleteServletTest extends Mockito {

	@Test
	public void PatientDeleteServletTest() throws IOException, ServletException {
		HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);    

        when(request.getParameter("PatientID")).thenReturn("1");
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);

        new PatientDeleteServlet().doPost(request, response);
        
        verify(request, atLeast(1)).getParameter("PatientID"); 
        writer.flush(); 
        assertTrue(stringWriter.toString().contains("has been deleted!"));

	}

}
