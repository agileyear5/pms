package pms.test;

import static org.junit.Assert.*;

import org.junit.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import junit.framework.TestCase;
import pms.servlets.gui.GPServlet;
import pms.servlets.gui.UpdateServlet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class UpdateStaffServletTest extends Mockito {

	/*
	 * @Mock HttpServletRequest request;
	 * 
	 * @Mock HttpServletResponse response;
	 * 
	 * @Mock HttpSession session;
	 * 
	 * @Before protected void setUp() throws Exception {
	 * MockitoAnnotations.initMocks(this); }
	 */

	@Test
	public void testDoPostHttpServletRequestHttpServletResponse() throws Exception {

		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);

		when(request.getParameter("id")).thenReturn("3");
		when(request.getParameter("firstname")).thenReturn("xu");
		when(request.getParameter("surname")).thenReturn("luo");
		when(request.getParameter("password")).thenReturn("agile2017");
		when(request.getParameter("address")).thenReturn("Merrion Sq., Dublin");
		when(request.getParameter("telephone")).thenReturn("018623512");
		when(request.getParameter("email")).thenReturn("xu.luo@student.ait.ie");
		when(request.getParameter("role")).thenReturn("PHARMACIST");
		StringWriter stringWriter = new StringWriter();
		PrintWriter writer = new PrintWriter(stringWriter);
		when(response.getWriter()).thenReturn(writer);

		new UpdateServlet().doPost(request, response);

		verify(request, atLeast(1)).getParameter("id");
		verify(request, atLeast(1)).getParameter("firstname");
		verify(request, atLeast(1)).getParameter("surname");
		verify(request, atLeast(1)).getParameter("password");
		verify(request, atLeast(1)).getParameter("address");
		verify(request, atLeast(1)).getParameter("telephone");
		verify(request, atLeast(1)).getParameter("email");
		verify(request, atLeast(1)).getParameter("role");
		writer.flush();
		assertTrue(stringWriter.toString().contains("Update staff information successfully!"));

		/*
		 * when(request.getParameter("id")).thenReturn("3");
		 * when(request.getParameter("firstname")).thenReturn("xu");
		 * when(request.getParameter("surname")).thenReturn("luo");
		 * when(request.getParameter("password")).thenReturn("agile2018");
		 * when(request.getParameter("role")).thenReturn("PHARMACIST");
		 * when(request.getSession()).thenReturn(session);
		 * 
		 * StringWriter sw = new StringWriter(); PrintWriter pw = new
		 * PrintWriter(sw);
		 * 
		 * when(response.getWriter()).thenReturn(pw);
		 * 
		 * new UpdateServlet().doPost(request, response);
		 * 
		 * Verify the session attribute value verify(session).setAttribute("id",
		 * "3"); verify(session).setAttribute("id", "3");
		 * verify(session).setAttribute("surname", "luo");
		 * verify(session).setAttribute("password", "agile2018");
		 * verify(session).setAttribute("role", "PHARMACIST");
		 * 
		 * verify(rd).forward(request, response);
		 * 
		 * String result = sw.getBuffer().toString().trim();
		 * 
		 * System.out.println("Result: " + result);
		 * 
		 * assertEquals("<table><tr><td>Update staff information successfully!</td></tr>"
		 * , result); assertTrue(sw.toString().
		 * contains("<table><tr><td>Update staffinformation successfully!</td></tr>"
		 * ));
		 */
	}

}
