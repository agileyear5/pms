package pms.test;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.List;
import java.util.Properties;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.Statement;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import ie.ait.eng.sw.msc.agile.dao.Conn_db;
import ie.ait.eng.sw.msc.agile.dao.ExceptionHandlerClass;
import ie.ait.eng.sw.msc.agile.dao.PatientsDAO;
import ie.ait.eng.sw.msc.agile.util.Patients;
import ie.ait.eng.sw.msc.agile.util.Staff;
import junit.framework.Assert;

public class UpdatePatientTest {


	private static PatientsDAO dao;
	private static Conn_db con;
	private Patients patients = new Patients();
	private int testID=1;
    private Properties prop = new Properties();
    private InputStream input = null;
    
	@BeforeClass
	public static void setup() throws Exception {
		dao = new PatientsDAO();
		
	}
	
	@Test
	public void testUpdatePatients0() throws ClassNotFoundException {
	
		patients.setId(testID);
		patients.setFirstName("liu");
		patients.setSecondName("mio");
		
		dao.update(patients);
		int id = patients.getId();
		
		Assert.assertNotNull(id);
		
		Patients updatepatient = dao.findPatient(id);
		
		Assert.assertEquals("liu", updatepatient.getFirstName());
		Assert.assertEquals("mio", updatepatient.getSecondName());
	}
     
	@Test
	public void testUpdatePatients1() throws ClassNotFoundException {
	
		patients.setId(testID);
		patients.setFirstName("liuddd");
		patients.setSecondName("mioddd");
		
		dao.update(patients);
		int id = patients.getId();
		
		Assert.assertNotNull(id);
		
		Patients updatepatient = dao.findPatient(id);
		
		Assert.assertEquals("liuddd", updatepatient.getFirstName());
		Assert.assertEquals("mioddd", updatepatient.getSecondName());
	}
	
	@Test
	public void testUpdatePatients2() throws ClassNotFoundException {
		
		patients.setId(testID);
		patients.setFirstName("liu");
		patients.setSecondName("mioeveeee");
		
		dao.update(patients);
		int id = patients.getId();
		
		Assert.assertNotNull(id);
		
		Patients updatepatient = dao.findPatient(id);
		
		Assert.assertEquals("liu", updatepatient.getFirstName());
		Assert.assertEquals("mioeveeee", updatepatient.getSecondName());
	}
	
	@Test
	public void testUpdatePatients3() throws ClassNotFoundException {
		
		patients.setId(testID);
		patients.setFirstName("");
		patients.setSecondName("mioeveeee");
		
		dao.update(patients);
		int id = patients.getId();
		
		Assert.assertNotNull(id);
		
		Patients updatepatient = dao.findPatient(id);
		
		Assert.assertEquals("", updatepatient.getFirstName());
		Assert.assertEquals("mioeveeee", updatepatient.getSecondName());
	}

	
	
	@Test
public void testUpdatePatients4() throws ClassNotFoundException {
		
		patients.setId(testID);
		patients.setFirstName("@");
		patients.setSecondName("mioeveeee");
		
		dao.update(patients);
		int id = patients.getId();
		
		Assert.assertNotNull(id);
		
		Patients updatepatient = dao.findPatient(id);
		
		Assert.assertEquals("@", updatepatient.getFirstName());
		Assert.assertEquals("mioeveeee", updatepatient.getSecondName());
	}
	
	@Test
public void testUpdatePatients5() throws ClassNotFoundException {
		
		patients.setId(testID);
		patients.setFirstName("liuee");
		patients.setSecondName("mio");
		
		dao.update(patients);
		int id = patients.getId();
		
		Assert.assertNotNull(id);
		
		Patients updatepatient = dao.findPatient(id);
		
		Assert.assertEquals("liuee", updatepatient.getFirstName());
		Assert.assertEquals("mio", updatepatient.getSecondName());
	}
	
	@Test
	public void testUpdatePatients6() throws ClassNotFoundException {
			
			patients.setId(testID);
			patients.setFirstName("liuee");
			patients.setSecondName("mio");
			
			dao.update(patients);
			int id = patients.getId();
			
			Assert.assertNotNull(id);
			
			Patients updatepatient = dao.findPatient(id);
			
			Assert.assertEquals("liuee", updatepatient.getFirstName());
			Assert.assertEquals("mio", updatepatient.getSecondName());
		}
	
	}