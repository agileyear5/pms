package pms.test;

import static org.junit.Assert.*;

import org.junit.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import junit.framework.TestCase;
import pms.servlets.gui.GPServlet;
import pms.servlets.gui.RegisterGPServlet;
import pms.servlets.gui.UpdateServlet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class AddGPTest extends Mockito {


	@Test
	public void testDoPostHttpServletRequestHttpServletResponse() throws Exception {

		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);

		when(request.getParameter("firstname")).thenReturn("dingcheng");	
		when(request.getParameter("password")).thenReturn("agile2017");
		StringWriter stringWriter = new StringWriter();
		PrintWriter writer = new PrintWriter(stringWriter);
		when(response.getWriter()).thenReturn(writer);

		new RegisterGPServlet().doPost(request, response);

		verify(request, atLeast(1)).getParameter("firstname");
		verify(request, atLeast(1)).getParameter("password");
		writer.flush();
		

		
	}

}
