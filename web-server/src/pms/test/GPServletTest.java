package pms.test;

import static org.junit.Assert.*;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

import pms.servlets.gui.GPServlet;

public class GPServletTest extends Mockito{

	@Test
	public void GPServletTest() throws Exception {
		HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);    

        when(request.getParameter("PatientOperation")).thenReturn("view All Patients");
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);
        new GPServlet().doGet(request, response);
        verify(request, atLeast(1)).getParameter("PatientOperation"); 
        writer.flush(); 
        assertTrue(stringWriter.toString().contains("<title>All Patients List</title>"));
	}
	

}
