package pms.test;

import static org.junit.Assert.*;

import org.junit.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import junit.framework.TestCase;
import pms.servlets.gui.CreatePatentServlet;
import pms.servlets.gui.CreatePrescriptionServlet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class CreatePatientTest extends Mockito {



	@Test
	public void testDoPostHttpServletRequestHttpServletResponse() throws Exception {

		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);

		when(request.getParameter("firstname")).thenReturn("lulu");
		when(request.getParameter("surname")).thenReturn("Alghamdi");
		when(request.getParameter("gender")).thenReturn("F");
		when(request.getParameter("address")).thenReturn("Athlone");
		when(request.getParameter("telephone")).thenReturn("055247859");
		when(request.getParameter("email")).thenReturn("lulu@student.ait.ie");
		StringWriter stringWriter = new StringWriter();
		PrintWriter writer = new PrintWriter(stringWriter);
		when(response.getWriter()).thenReturn(writer);

		new CreatePatentServlet().doPost(request, response);


		verify(request, atLeast(1)).getParameter("firstname");
		verify(request, atLeast(1)).getParameter("surname");
		verify(request, atLeast(1)).getParameter("gender");
		verify(request, atLeast(1)).getParameter("address");
		verify(request, atLeast(1)).getParameter("telephone");
		verify(request, atLeast(1)).getParameter("email");
	
		writer.flush();
		assertTrue(stringWriter.toString().contains("<table><tr><td>patient inserted successfully!</td></tr>"));


	}

}