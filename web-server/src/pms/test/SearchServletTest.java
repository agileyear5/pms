package pms.test;

import static org.junit.Assert.assertTrue;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import ie.ait.eng.sw.msc.agile.dao.PatientsDAO;
import ie.ait.eng.sw.msc.agile.dao.PharDAO;
import ie.ait.eng.sw.msc.agile.dao.PrescriptionDAO;
import ie.ait.eng.sw.msc.agile.dao.StaffDAO;
import ie.ait.eng.sw.msc.agile.util.Patients;
import ie.ait.eng.sw.msc.agile.util.Prescription;
import ie.ait.eng.sw.msc.agile.util.Staff;
import ie.ait.eng.sw.msc.agile.util.pharmacist;
import junit.framework.Assert;
import pms.servlets.gui.PatientSearchServlet;
import pms.servlets.gui.SearchServlet;
import pms.servlets.gui.UpdateServlet;

public class SearchServletTest extends Mockito{
	private static StaffDAO staffDAO;
	private Staff staff = null;
	private int testID = 3;
	private String TestString = "1";
	private StaffDAO DAO = null;
	private static PatientsDAO PDAO = null;
	private Patients patient = null;
	private pharmacist pharmacist = null;
	private static PharDAO PHDAO = null;
	private static PrescriptionDAO PRDAO = null;
	private Prescription prescription = null;
	
	@BeforeClass
	public static void Staffsetup() throws Exception {
		staffDAO = new StaffDAO();
		PHDAO = new PharDAO();
		PRDAO = new PrescriptionDAO();
		PDAO = new PatientsDAO();
	}

	@Test
	public void StafftestSearch() {
		Staff staff = staffDAO.search(testID);
		Assert.assertEquals(testID, staff.getId());
		Assert.assertEquals("Xu", staff.getFirstName());
		Assert.assertEquals("Luo", staff.getSurname());
		Assert.assertEquals("agile2017", staff.getPassword());
		Assert.assertEquals("PHARMACIST", staff.getRole());
		System.out.println("id:" + staff.toString());
		return;
	}

	@Test
	public void StaffTestSearchEngine() {
		Staff staff = staffDAO.searchEngine("Des");
		Assert.assertEquals(1, staff.getId());
		Assert.assertEquals("Des", staff.getFirstName());
		Assert.assertEquals("Howlin", staff.getSurname());
		Assert.assertEquals("agile2017", staff.getPassword());
		Assert.assertEquals("Dublin Road, Athlone", staff.getAddress());
		Assert.assertEquals("0906495431", staff.getTelephone());
		Assert.assertEquals("des.howlin@student.ait.ie", staff.getEmail());
		Assert.assertEquals("ADMIN", staff.getRole());
		System.out.println("id:" + staff.toString());
		return;
	}


	@Test
	public void PatientestSearch() throws ClassNotFoundException {
		Patients p = PDAO.findPatient(testID);
		Assert.assertEquals(testID, p.getId());
		Assert.assertEquals("Li", p.getFirstName());
		Assert.assertEquals("Liu", p.getSecondName());	
		System.out.println("id:" + p.toString());
		return;
	}
	
	@Test
	public void PhtestSearch() {
		pharmacist = PHDAO.search(1);
		Assert.assertEquals(1, pharmacist.getID());
		Assert.assertEquals("xu", pharmacist.getfirstname());
		Assert.assertEquals("luo", pharmacist.getPassword());
		Assert.assertEquals("Woodville, Athlone", pharmacist.getAddress());
		Assert.assertEquals(null, pharmacist.getTelephone());
		Assert.assertEquals("xu.luo@student.ait.ie", pharmacist.getEmail());
		System.out.println("id:" + pharmacist.toString());
		return;
	}
	
	@Test
	public void PResSearch() {
		prescription = PRDAO.search(1);
		Assert.assertEquals(3, prescription.getId());
		Assert.assertEquals(1, prescription.getPatientId());
		Assert.assertEquals("Sinutab", prescription.getPrescription());
		Assert.assertEquals("none", prescription.getAllergies());
		Assert.assertEquals("may cause drowsiness", prescription.getAdvice_info());
		Assert.assertEquals(null, prescription.getComments());
		System.out.println("id:" + prescription.toString());
		return;
	}
	
	@Test
	public void testDoPostHttpServletRequestHttpServletResponse() throws Exception {

	HttpServletRequest request = mock(HttpServletRequest.class);
	HttpServletResponse response = mock(HttpServletResponse.class);

	when(request.getParameter("pid")).thenReturn("3");
	StringWriter stringWriter = new StringWriter();
	PrintWriter writer = new PrintWriter(stringWriter);
	when(response.getWriter()).thenReturn(writer);

	new PatientSearchServlet().doGet(request, response);

	verify(request, atLeast(1)).getParameter("pid");
	writer.flush();
	assertTrue(stringWriter.toString().contains("<table><tr><td>ID</td><td>"));


	}

}
