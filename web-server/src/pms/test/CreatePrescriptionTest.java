package pms.test;

import static org.junit.Assert.*;

import org.junit.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import junit.framework.TestCase;

import pms.servlets.gui.CreatePrescriptionServlet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class CreatePrescriptionTest extends Mockito {



	@Test
	public void testDoPostHttpServletRequestHttpServletResponse() throws Exception {

		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);

		when(request.getParameter("pid")).thenReturn("2");
		when(request.getParameter("pres")).thenReturn("get cold");
		when(request.getParameter("all")).thenReturn("take 4 a day");
		when(request.getParameter("adv")).thenReturn("more sleep");
		StringWriter stringWriter = new StringWriter();
		PrintWriter writer = new PrintWriter(stringWriter);
		when(response.getWriter()).thenReturn(writer);

		new CreatePrescriptionServlet().doPost(request, response);

		verify(request, atLeast(1)).getParameter("pid");
		verify(request, atLeast(1)).getParameter("pres");
		verify(request, atLeast(1)).getParameter("all");
		verify(request, atLeast(1)).getParameter("adv");
	
		writer.flush();
		assertTrue(stringWriter.toString().contains("Add prescription information successfully!"));


	}

}