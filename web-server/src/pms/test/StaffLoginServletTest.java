package pms.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

import pms.servlets.gui.StaffLoginServlet;

public class StaffLoginServletTest extends Mockito {
	
	@Test
	public void StaffLoginServletTest1() throws Exception {
		HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);    

        when(request.getParameter("UserName")).thenReturn("Wei");
        when(request.getParameter("SurName")).thenReturn("Hu");
        when(request.getParameter("Password")).thenReturn("agile2017");
        when(request.getParameter("role")).thenReturn("GP");
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);

        new StaffLoginServlet().doPost(request, response);
        
        verify(request, atLeast(1)).getParameter("UserName");
        verify(request, atLeast(1)).getParameter("SurName"); 
        verify(request, atLeast(1)).getParameter("Password"); 
        verify(request, atLeast(1)).getParameter("role"); 
        writer.flush(); 
        assertTrue(stringWriter.toString().contains("login success"));
	}

	@Test
	public void StaffLoginServletTest0() throws Exception {
		HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);    

        when(request.getParameter("UserName")).thenReturn("Xu");
        when(request.getParameter("SurName")).thenReturn("Luo");
        when(request.getParameter("Password")).thenReturn("agile2017");
        
        when(request.getParameter("role")).thenReturn("PHARMACIST");
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);

        new StaffLoginServlet().doPost(request, response);
        
        verify(request, atLeast(1)).getParameter("UserName");
        verify(request, atLeast(1)).getParameter("SurName"); 
        verify(request, atLeast(1)).getParameter("Password"); 
         
        verify(request, atLeast(1)).getParameter("role"); 
        writer.flush(); 
    

	}
	
	
        @Test
    	public void StaffLoginServletTest2() throws Exception {
    		HttpServletRequest request = mock(HttpServletRequest.class);       
            HttpServletResponse response = mock(HttpServletResponse.class);    

            when(request.getParameter("UserName")).thenReturn("Des");
            when(request.getParameter("SurName")).thenReturn("Howlin");
            when(request.getParameter("Password")).thenReturn("agile2017");
            when(request.getParameter("role")).thenReturn("admin");
            StringWriter stringWriter = new StringWriter();
            PrintWriter writer = new PrintWriter(stringWriter);
            when(response.getWriter()).thenReturn(writer);

            new StaffLoginServlet().doPost(request, response);
            
            verify(request, atLeast(1)).getParameter("UserName");
            verify(request, atLeast(1)).getParameter("SurName"); 
            verify(request, atLeast(1)).getParameter("Password"); 
            verify(request, atLeast(1)).getParameter("role"); 
            writer.flush(); 
        
	}
    

        @Test
    	public void StaffLoginServletTest3() throws Exception {
    		HttpServletRequest request = mock(HttpServletRequest.class);       
            HttpServletResponse response = mock(HttpServletResponse.class);    

            when(request.getParameter("UserName")).thenReturn("Hui");
            when(request.getParameter("SurName")).thenReturn("Yuan");
            when(request.getParameter("Password")).thenReturn("agile2017");
            when(request.getParameter("role")).thenReturn("OTHER");
            StringWriter stringWriter = new StringWriter();
            PrintWriter writer = new PrintWriter(stringWriter);
            when(response.getWriter()).thenReturn(writer);

            new StaffLoginServlet().doPost(request, response);
            
            verify(request, atLeast(1)).getParameter("UserName");
            verify(request, atLeast(1)).getParameter("SurName"); 
            verify(request, atLeast(1)).getParameter("Password"); 
            verify(request, atLeast(1)).getParameter("role"); 
            writer.flush(); 
        
	}
        @Test
    	public void StaffLoginServletTest4() throws Exception {
    		HttpServletRequest request = mock(HttpServletRequest.class);       
            HttpServletResponse response = mock(HttpServletResponse.class);    

            when(request.getParameter("UserName")).thenReturn("Hui");
      
            when(request.getParameter("Password")).thenReturn("agile2017");
    
    
            when(request.getParameter("role")).thenReturn("OTHER");
            StringWriter stringWriter = new StringWriter();
            PrintWriter writer = new PrintWriter(stringWriter);
            when(response.getWriter()).thenReturn(writer);

            new StaffLoginServlet().doPost(request, response);
            
            verify(request, atLeast(1)).getParameter("UserName");
            verify(request, atLeast(1)).getParameter("Password");
      
            verify(request, atLeast(1)).getParameter("role"); 
            writer.flush(); 
        
	}
        
}
