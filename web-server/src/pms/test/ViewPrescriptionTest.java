package pms.test;


import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import ie.ait.eng.sw.msc.agile.dao.PrescriptionDAO;
import ie.ait.eng.sw.msc.agile.util.Prescription;

import junit.framework.Assert;

public class ViewPrescriptionTest {
	private static PrescriptionDAO prescriptionDAO;
	private Prescription prescription ;
	private int testID =1;

	@BeforeClass
	public static void setup() throws Exception {
		prescriptionDAO = new PrescriptionDAO();
	}

	@Test
	public void testSearch0() {

		Prescription prescription = prescriptionDAO.search(testID);
		Assert.assertEquals(testID, prescription.getId());
		Assert.assertEquals("drink water",prescription.getAdvice_info());
		
		return;
	}

	    
	@Test
	public void testSearch1() {
		Prescription prescription = prescriptionDAO.search(testID);
		Assert.assertEquals(testID, prescription.getId());
		
		Assert.assertEquals("take 2 a day",prescription.getAllergies());
		
		Assert.assertEquals("drink water",prescription.getAdvice_info());
		
		
		
		
		return;
	}
	
	@Test
	public void testSearch2() {
		Prescription prescription = prescriptionDAO.search(testID);
		Assert.assertEquals(testID, prescription.getId());
		
		Assert.assertEquals("PainKillers",prescription.getPrescription());
	
		Assert.assertEquals("take 2 a day",prescription.getAllergies());
		
		Assert.assertEquals("drink water",prescription.getAdvice_info());
		
		
		
		return;
	}

}
