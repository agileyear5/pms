package pms.test;

import org.junit.Test;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pms.servlets.gui.RegisterPharServlet;
import org.mockito.Mockito;

public class AddPharTest extends Mockito {


	@Test
	public void testDoPostHttpServletRequestHttpServletResponse() throws Exception {

		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);

		when(request.getParameter("firstname")).thenReturn("dingcheng");	
		when(request.getParameter("password")).thenReturn("agile2017");
		StringWriter stringWriter = new StringWriter();
		PrintWriter writer = new PrintWriter(stringWriter);
		when(response.getWriter()).thenReturn(writer);

		new RegisterPharServlet().doPost(request, response);

		verify(request, atLeast(1)).getParameter("firstname");
		verify(request, atLeast(1)).getParameter("password");
		writer.flush();
		

		
	}

}
