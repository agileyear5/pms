package pms.test;

import static org.junit.Assert.*;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import ie.ait.eng.sw.msc.agile.dao.PrescriptionDAO;
import ie.ait.eng.sw.msc.agile.util.Prescription;
import junit.framework.Assert;
import pms.servlets.gui.PatientSearchServlet;

public class SearchPrescriptionByIdTest extends Mockito{
	//private int testID = 3;
	private static PrescriptionDAO PRDAO = null;
	private Prescription prescription = null;
	
	@BeforeClass
	public static void setup() throws Exception {		
		PRDAO = new PrescriptionDAO();
	}
	
	@Test
	public void prescriptionSearchByIdTest() {
		prescription = PRDAO.search(3);
		Assert.assertEquals(1, prescription.getId());
		Assert.assertEquals(3, prescription.getPatientId());
		Assert.assertEquals("Painkillers", prescription.getPrescription());
		Assert.assertEquals("take 2 a day", prescription.getAllergies());
		Assert.assertEquals(null, prescription.getAdvice_info());
		Assert.assertEquals(null, prescription.getComments());
		//System.out.println("id:" + prescription.toString());
		return;
	}

	@Test
	public void testDoPostHttpServletRequestHttpServletResponse() throws Exception {

	HttpServletRequest request = mock(HttpServletRequest.class);
	HttpServletResponse response = mock(HttpServletResponse.class);

	when(request.getParameter("pid")).thenReturn("3");
	StringWriter stringWriter = new StringWriter();
	PrintWriter writer = new PrintWriter(stringWriter);
	when(response.getWriter()).thenReturn(writer);

	new PatientSearchServlet().doGet(request, response);

	verify(request, atLeast(1)).getParameter("pid");
	writer.flush();
	assertTrue(stringWriter.toString().contains("<table><tr><td>ID</td><td>"));


	}

}
