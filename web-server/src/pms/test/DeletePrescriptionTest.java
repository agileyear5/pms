package pms.test;

import org.junit.Test;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import pms.servlets.gui.DeletePrescriptionServlet;
import org.mockito.Mockito;

public class DeletePrescriptionTest extends Mockito {


	@Test
	public void testDoPostHttpServletRequestHttpServletResponse() throws Exception {

		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		when(request.getParameter("pid")).thenReturn("1");		
		StringWriter stringWriter = new StringWriter();
		PrintWriter writer = new PrintWriter(stringWriter);
		when(response.getWriter()).thenReturn(writer);
		new DeletePrescriptionServlet().doPost(request, response);
		verify(request, atLeast(1)).getParameter("pid");	
		writer.flush();
		

		
	}

}