package pms.test;

import org.junit.Test;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pms.servlets.gui.DeleteGP;
import pms.servlets.gui.RegisterGPServlet;
import org.mockito.Mockito;

public class DeleteGPTest extends Mockito {


	@Test
	public void testDoPostHttpServletRequestHttpServletResponse() throws Exception {

		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		when(request.getParameter("id")).thenReturn("1");		
		StringWriter stringWriter = new StringWriter();
		PrintWriter writer = new PrintWriter(stringWriter);
		when(response.getWriter()).thenReturn(writer);
		new DeleteGP().doPost(request, response);
		verify(request, atLeast(1)).getParameter("id");	
		writer.flush();
		

		
	}

}
