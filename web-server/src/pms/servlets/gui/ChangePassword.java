package pms.servlets.gui;

	import java.io.IOException;
	import java.io.PrintWriter;
	import java.sql.DriverManager;
	import java.sql.ResultSet;
	import java.sql.SQLException;

	import javax.servlet.ServletException;
	import javax.servlet.annotation.WebServlet;
	import javax.servlet.http.HttpServlet;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;

	import ie.ait.eng.sw.msc.agile.util.Staff;

	import java.sql.Connection;
	import java.sql.Statement;
	@WebServlet(name = "ChangePassword", urlPatterns = { "/ChangePassword" })
	public class ChangePassword extends HttpServlet {
		private static final long serialVersionUID = 1L;

		private String url;
		private String driver = null;

		@Override
		public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		}

		@Override
		public void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			Connection connection = null; 
			Statement statement = null;
			// ResultSet resultset=null;

			url = "jdbc:mysql://localhost:3306/PMS?user=admin&password=admin";
			driver = "com.mysql.jdbc.Driver";
			try {
				Class.forName(driver);
				connection = DriverManager.getConnection(url);
			} catch (ClassNotFoundException | SQLException ex) {
				System.out.println("Error: unable to load driver class!");
				System.exit(1);
			}

			try {
				String oldpassword = request.getParameter("oldpassword");	
				String newpassword = request.getParameter("newpassword");
				String update = "UPDATE pms.staff SET pass = '"+newpassword+"' WHERE pass='"+oldpassword+"'and role='ADMIN';";
				statement = connection.createStatement();
				statement.executeUpdate(update);
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				out.println("<table><tr><td>Change Password  successfully!</td></tr>");
				out.println("<tr><td><a href=\"Admin.html\">Return Main Menu</a></td></tr>");
				statement.close();
				connection.close();
			} catch (SQLException se) {
				// Handle errors for JDBC
				se.printStackTrace();
			} catch (Exception e) {
				// Handle errors for Class.forName
				e.printStackTrace();
			}
		}
  
	}