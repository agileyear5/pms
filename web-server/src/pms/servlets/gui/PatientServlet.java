package pms.servlets.gui;

import java.io.IOException;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

@WebServlet(name = "PatientServlet", urlPatterns = { "/PatientServlet" })
public class PatientServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private String url;
	private String driver = null;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Connection connection = null;
		url = "jdbc:mysql://localhost:3306/PMS?user=admin&password=admin";
		driver = "com.mysql.jdbc.Driver";
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url);
		} catch (ClassNotFoundException | SQLException ex) {
			System.out.println("Error: unable to load driver class!");
			System.exit(1);
		}

		try {
			
			String sql = "SELECT * FROM pms.prescriptions;";
			Statement statement = connection.createStatement();
			ResultSet resultset = statement.executeQuery(sql);
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<table><tr><td>ID</td><td>PatientID<td>Prescription</td><td>Allergies</td><td>Advice Info</td><td>Comments</td></tr>");
			while (resultset.next()) {
			    String id = resultset.getString("id");
			    String PatientID = resultset.getString("PatientID");
				String Prescription= resultset.getString("Prescription");
				String Allergies = resultset.getString("Allergies");
				String Advice_Info = resultset.getString("Advice_Info");
				String Comments = resultset.getString("Comments");
				
				
				out.println("<FORM ACTION=\"PatientServlet\" METHOD=\"POST\" id =\""+id+"\">");
				out.println("<tr><td>"+ "<input type=\"text\" name=\"id\" id=\"id\" value=\"" + id + "\" readonly>" 
						+ "</td><td>" + "<input type=\"text\" name=\"PatientID\" id=\"PatientID\" value=\"" + PatientID + "\">" 
						+ "</td><td>" + "<input type=\"text\" name=\"Prescription\" id=\"Prescription\" value=\"" + Prescription + "\">" 
						+ "</td><td>" + "<input type=\"text\" name=\"Allergies\" id=\"Allergies\" value=\"" + Allergies + "\">" 
	                   + "</td><td>" + "<input type=\"text\" name=\"Advice Info\" id=\"Advice Info\" value=\"" + Advice_Info + "\">"
	                   + "</td><td>" + "<input type=\"text\" name=\"Comments\" id=\"Advice Info\" value=\"" + Comments + "\">"
	                   
						);
				out.println("</FORM>");
				out.println("</table>");
				
			
				
			}
		
			resultset.close();
			statement.close();
			connection.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}
	} // end try

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
