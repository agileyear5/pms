package pms.servlets.gui;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ie.ait.eng.sw.msc.agile.dao.Conn_db;
import ie.ait.eng.sw.msc.agile.dao.ExceptionHandlerClass;

import ie.ait.eng.sw.msc.agile.dao.PatientsDAO;
import ie.ait.eng.sw.msc.agile.dao.PharDAO;
import ie.ait.eng.sw.msc.agile.dao.PrescriptionDAO;
import ie.ait.eng.sw.msc.agile.dao.StaffDAO;
import ie.ait.eng.sw.msc.agile.dao.impl.DbConnection;
import ie.ait.eng.sw.msc.agile.util.Patients;
import ie.ait.eng.sw.msc.agile.util.Prescription;
import ie.ait.eng.sw.msc.agile.util.Staff;
import ie.ait.eng.sw.msc.agile.util.pharmacist;

import javax.servlet.annotation.WebServlet;

@WebServlet(name = "PharmacistSearchServlet", urlPatterns = { "/PharmacistSearchServlet" })
public class PharmacistSearchServlet extends HttpServlet {
	
	Staff staff = null;
	StaffDAO DAO = null;
	PatientsDAO PDAO = null;
	Patients patient = null;
	pharmacist pharmacist = null;
	PharDAO PHDAO = null;
	PrescriptionDAO PRDAO = null;
	Prescription prescription = null;
	PrintWriter out = null;
	private static final long serialVersionUID = 1L;
	
	public void WritePage(String role,String ID,PrintWriter out, HttpServletResponse response) {
	
	
	try {
	
	
		if  (ID == "") {
			response.setContentType("text/html");
			out.println("there is no user with 0 ID"+ "<FORM ACTION=\"Search.html\" METHOD=\"POST\">" +
				"<input type=\"submit\" name=\"submit\" value=\"Search\">"
						+ "</FORM>");
			
			}else if (role == null){

				PDAO = new PatientsDAO();
					patient = PDAO.SearchEngine(ID);
					response.setContentType("text/html");
					
					out.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">");
					out.println("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>>");
					out.println("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>");
					out.println("<table class=\"table\" ><tr><td>ID</td><td>First Name</td><td>Surname</td><td>Gender</td><td>Address</td><td>Telephone</td><td>Email</td></tr>");
					out.println("<FORM ACTION=\"PatientUpdateServlet\" METHOD=\"POST\" id =\"" + patient.getId() + "\">");
					out.println("<tr><td>" + "<input class=\"form-control\" type=\"text\" name=\"id\" id=\"id\" value=\"" + patient.getId() + "\" readonly>" + "</td><td>"
							+ "<input type=\"text\" class=\"form-control\" name=\"firstname\" id=\"firstname\" value=\"" + patient.getFirstName()+ "\">" + "</td><td>" 
							+ "<input type=\"text\"  class=\"form-control\" name=\"surname\" id=\"surname\" value=\"" + patient.getSecondName() + "\">" + "</td><td>"
							//+ "<input type=\"text\" name=\"gender\" id=\"gender\" value=\"" + patient.getGender() + "\">" + "</td><td>"
							+"<select class=\"form-control\" name = \"gender\" id=\"gender\">"
							+ "<option value =\""+ patient.getGender() + "\" id=\"role\" selected>" + patient.getGender() + "</option>"
							+ "<option value ='F'>F</option>" 
							+ "<option value ='M'>M</option>"
							+ "</select></td><td>" 
							+ "<input type=\"text\"  class=\"form-control\" name=\"address\" id=\"address\" value=\"" + patient.getAddress() + "\">" + "</td><td>"
							+ "<input type=\"text\"  class=\"form-control\" name=\"telephone\" id=\"telephone\" value=\"" + patient.getTelephone() + "\">" + "</td><td>"
							+ "<input type=\"text\"  class=\"form-control\" name=\"email\" id=\"email\" value=\"" + patient.getEmail() + "\"></td><td>"
							+ "<tr><td><input class=\"btn btn-danger\" type=\"submit\" name=\"submit\" value=\"Update\"></td></tr>");
					out.println("</FORM>");
					out.println("</table>");
					
					
					
				}
				
			
	     	switch (role) {
		
        	case "Patient" :

			PDAO = new PatientsDAO();
			patient = PDAO.SearchEngine(ID);
			response.setContentType("text/html");
			
			out.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">");
			out.println("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>>");
			out.println("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>");
			out.println("<table class=\"table\" ><tr><td>ID</td><td>First Name</td><td>Surname</td><td>Gender</td><td>Address</td><td>Telephone</td><td>Email</td></tr>");
			out.println("<FORM ACTION=\"PatientUpdateServlet\" METHOD=\"POST\" id =\"" + patient.getId() + "\">");
			out.println("<tr><td>" + "<input class=\"form-control\" type=\"text\" name=\"id\" id=\"id\" value=\"" + patient.getId() + "\" readonly>" + "</td><td>"
					+ "<input type=\"text\" class=\"form-control\" name=\"firstname\" id=\"firstname\" value=\"" + patient.getFirstName()+ "\">" + "</td><td>" 
					+ "<input type=\"text\"  class=\"form-control\" name=\"surname\" id=\"surname\" value=\"" + patient.getSecondName() + "\">" + "</td><td>"
					//+ "<input type=\"text\" name=\"gender\" id=\"gender\" value=\"" + patient.getGender() + "\">" + "</td><td>"
					+"<select class=\"form-control\" name = \"gender\" id=\"gender\">"
					+ "<option value =\""+ patient.getGender() + "\" id=\"role\" selected>" + patient.getGender() + "</option>"
					+ "<option value ='F'>F</option>" 
					+ "<option value ='M'>M</option>"
					+ "</select></td><td>" 
					+ "<input type=\"text\"  class=\"form-control\" name=\"address\" id=\"address\" value=\"" + patient.getAddress() + "\">" + "</td><td>"
					+ "<input type=\"text\"  class=\"form-control\" name=\"telephone\" id=\"telephone\" value=\"" + patient.getTelephone() + "\">" + "</td><td>"
					+ "<input type=\"text\"  class=\"form-control\" name=\"email\" id=\"email\" value=\"" + patient.getEmail() + "\"></td><td>"
					+ "<tr><td><input class=\"btn btn-danger\" type=\"submit\" name=\"submit\" value=\"Update\"></td></tr>");
			out.println("</FORM>");
			out.println("</table>");
			
			break;
			
        	case "Prescription" : 

			PRDAO = new PrescriptionDAO();
			prescription = PRDAO.SearchEngine(ID);
			response.setContentType("text/html");
			out.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">");
			out.println("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>>");
			out.println("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>");
			
			out.println("<table class=\"table\" ><tr><td>ID</td><td>Patient ID</td><td>PRESCRIPTION</td><td>ALLERGIES</td><td>comments</td><td>ADVICE_INFO</td>");
			out.println("<FORM ACTION=\"PharmacistServlet\" METHOD=\"POST\" id =\"" + prescription.getPatientId() + "\">");
			out.println("<tr><td>" + "<input class=\"form-control\"  type=\"text\" name=\"id\" id=\"id\" value=\"" + prescription.getId()+ "\" readonly>" + "</td><td>" 
					+ "<input class=\"form-control\"  type=\"text\" name=\"pid\" id=\"pid\" value=\"" + prescription.getPatientId() + "\" readonly>"
					+ "<input class=\"form-control\"  type=\"text\" name=\"prescription\" id=\"prescription\" value=\"" + prescription.getPrescription()+ "\">" + "</td><td>" 
					+ "<input class=\"form-control\"  type=\"text\" name=\"allergies\" id=\"allergies\" value=\"" + prescription.getAllergies()	+ "\">" + "</td><td>"  
					+ "<input class=\"form-control\"  type=\"text\" name=\"comments\" id=\"comments\" value=\"" + prescription.getComments()	+ "\">" + "</td><td>" 
					+ "<input class=\"form-control\"  type=\"text\" name=\"advice_info\" id=\"advice_info\" value=\""	+ prescription.getAdvice_info() + "\">" + "</td><td>"
					+ "<tr><td><input class=\"btn btn-danger\"  type=\"submit\" name=\"submit\" value=\"Update\"></td></tr>");
			
			
			break;
			
			}
			
	} catch (Exception e) {

		e.printStackTrace();
	}
}
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, NumberFormatException,NumberFormatException {
	    String role = request.getParameter("role");
		String ID = request.getParameter("pid");
		out =  response.getWriter();
		WritePage(role,ID,out,response);
	}
	
	

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
		
	
	}
	
}
