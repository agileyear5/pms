package pms.servlets.gui;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ie.ait.eng.sw.msc.agile.dao.impl.PrescriptionManagementDAOImpl;
import ie.ait.eng.sw.msc.agile.util.Patients;

/**
 * Servlet implementation class PatientLoginServlet
 */
@WebServlet(name="/PatientLoginServlet",urlPatterns = {"/PatientLoginServlet"})
public class PatientLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PrescriptionManagementDAOImpl dao = new PrescriptionManagementDAOImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PatientLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		
		
		//Patients p=new Patients(id, firstname, surname, password, address, telephone, email, role);
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String FirstName = request.getParameter("FirstName");
		String SurName = request.getParameter("Surname");
		
		int paitientID=dao.getID(FirstName, SurName);
	

		//out.println("User Name is:" +UserName+", Sur name is: "+SurName+", Password is: " +Password);
		if (FirstName == null || SurName ==null)
			out.println("UserName or SurName is null, pls try again");
		else 
			if (dao.PatientLogin(FirstName, SurName)){
				
				/*out.println("<p>login success, page will redirect to gp page in following 2 sec</p>"
    					+ "<tr><td><a href=\"Patient.html\">Enter patient page</a></td></tr>");*/
				
				out.println("<html>");
				out.println("<body>");
				out.println("Hello, " + "  " + FirstName + " "+SurName+", "+"welcome !");
				out.println("<br>"+"<br>");
				out.println("<tr><td>"+"Your EmergenyID is : " + paitientID + "<br>"+"<br>");
				out.println("</body></html>");
				out.println("<tr><td><a href=\"Patient.html\">Enter patient page</a></td></tr>");
				
    			//response.setHeader("refresh","2;URL=Patient.html");
			}
			else 
				out.println("UserName or SurName or Password or role is not match, pls try again");
		
	}

}
