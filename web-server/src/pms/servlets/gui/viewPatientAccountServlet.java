package pms.servlets.gui;

import java.io.IOException;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

@WebServlet(name = "viewPatientAccountServlet", urlPatterns = { "/viewPatientAccountServlet" })
public class viewPatientAccountServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private String url;
	private String driver = null;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Connection connection = null;
		url = "jdbc:mysql://localhost:3306/PMS?user=admin&password=admin";
		driver = "com.mysql.jdbc.Driver";
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url);
		} catch (ClassNotFoundException | SQLException ex) {
			System.out.println("Error: unable to load driver class!");
			System.exit(1);
		}

		try {
			
			String sql = "SELECT * FROM pms.patient;";
			Statement statement = connection.createStatement();
			ResultSet resultset = statement.executeQuery(sql);
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<table><tr><td>ID</td><td>FirstName<td>SurName</td><td>Gender</td><td>Address</td><td>Telephone</td><td>Email</td></tr>");
			while (resultset.next()) {
			    String id = resultset.getString("ID");
			    String FirstName = resultset.getString("FIRSTNAME");
				String SurName= resultset.getString("SURNAME");
				String Gender = resultset.getString("GENDER");
				String Address = resultset.getString("ADDRESS");
				String Telephone = resultset.getString("TELEPHONE");
				String Email = resultset.getString("EMAIL");
				
				out.println("<FORM ACTION=\"viewPatientAccountServlet\" METHOD=\"POST\" id =\""+id+"\">");
				out.println("<tr><td>"+ "<input type=\"text\" name=\"id\" id=\"id\" value=\"" + id + "\" readonly>" 
						+ "</td><td>" + "<input type=\"text\" name=\"FirstName\" id=\"FirstName\" value=\"" + FirstName + "\">" 
						+ "</td><td>" + "<input type=\"text\" name=\"SurName\" id=\"SurName\" value=\"" + SurName + "\">" 
						+ "</td><td>" + "<input type=\"text\" name=\"Gender\" id=\"Gender\" value=\"" + Gender + "\">" 
	                   + "</td><td>" + "<input type=\"text\" name=\"Address\" id=\"Address\" value=\"" + Address + "\">"
	                   + "</td><td>" + "<input type=\"text\" name=\"Telephone\" id=\"Telephone\" value=\"" + Telephone + "\">"
	                   + "</td><td>" + "<input type=\"text\" name=\"Email\" id=\"Email\" value=\"" + Email + "\">"
						);
				out.println("</FORM>");
				out.println("</table>");
				
			
				
			}
		
			resultset.close();
			statement.close();
			connection.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}
	} // end try

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
