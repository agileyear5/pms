package pms.servlets.gui;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ie.ait.eng.sw.msc.agile.dao.PatientsDAO;
import ie.ait.eng.sw.msc.agile.dao.impl.PrescriptionManagementDAOImpl;
import ie.ait.eng.sw.msc.agile.util.Patients;

/**
 * Servlet implementation class GP
 */
@WebServlet(name="/GPServlet" ,urlPatterns = {"/GPServlet"})
public class GPServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private List<Patients> AllPatients;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GPServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			PatientsDAO pdao = new PatientsDAO();
			AllPatients = pdao.findAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
        out.println("<html>");    
        out.println("<head>");   
        out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"style/bootstrapV4.css\"/>\r\n" + 
        		"<link rel=\"stylesheet\" type=\"text/css\" href=\"style/font-awesome.css\"/>\r\n" + 
        		"<link rel=\"stylesheet\" type=\"text/css\" href=\"style/screen.css\"/>\r\n" + 
        		"<link rel=\"stylesheet\" href=\"rstyle/jquery.dataTables.css\">\r\n" + 
        		"<script type=\"text/javascript\" src=\"style/jquery.js\"></script>\r\n" + 
        		"<script type=\"text/javascript\" src=\"style/popper.min.js\"></script>\r\n" + 
        		"<script type=\"text/javascript\" src=\"style/jquery.dataTables.js\"></script>\r\n" + 
        		"<script type=\"text/javascript\" src=\"style/bootstrapV4.js\"></script>");
        
		String type = request.getParameter("PatientOperation");
		if (type.equals("view All Patients")) {
		
        out.println("<title>All Patients List</title>");    
        //out.println("<link href=\"style/GP.css\" rel=\"stylesheet\" type=\"text/css\">");        
        out.println("</head>");    
        out.println("<body>");    
		out.println("<header><h2>Patients list</h2></header>\r\n" + 
				"<section><div class=\"jumbotron\">"
				+ "<table class=\"table table-hover center-block\">\r\n" + 
				"<thead>\r\n" + 
				"<th>ID</th>\r\n" + 
				"<th>First Name</th>\r\n" + 
				"<th>Second Name</th>\r\n" +
				"<th>Gender</th>\r\n" +
				"<th>Address</th>\r\n" +
				"<th>Telephone</th>\r\n" +
				"<th>Email</th>\r\n" +
				"</thead><tbody>");
		for (int i=0; i < AllPatients.size();i++) {
			int id = AllPatients.get(i).getId();
			String FName =AllPatients.get(i).getFirstName();
			String SName = AllPatients.get(i).getSecondName();
			String Gender = AllPatients.get(i).getGender();
			String Address = AllPatients.get(i).getAddress();
			String Telephone = AllPatients.get(i).getTelephone();
			String Email = AllPatients.get(i).getEmail();
			
			out.println("<tr>\r\n" + 
					"<td>"+id +"</td>\r\n" + 
					"<td>"+FName+"</td>\r\n" + 
					"<td>"+SName+"</td>\r\n"+ 
					"<td>"+Gender+"</td>\r\n"+ 
					"<td>"+Address+"</td>\r\n"+ 
					"<td>"+Telephone+"</td>\r\n"+ 
					"<td>"+Email+"</td>\r\n"+ 
					"<td>"
					+ "<form action=\"PatientDeleteServlet\" method=\"post\" value="+id+">"
					+ "<button id=\"delete\" name=\"delete\" class=\"btn btn-primary btn-block\" type=\"submit\" value=\"delete Patient\">delete Patient</button>"
					+ "<input type=\"hidden\" name=\"PatientID\" id=\"delete\" value ="+id+">"
					+ "</form>"
					+ "</td>"
					/*+"<td>"
					+ "<form action=\"PatientUpdateServlet\" method=\"post\" value="+id+">"
					+ "<input type=\"submit\" name=\"update\" id=\"update\" value =\"update Patient\">"
					+ "<form action=\"PatientDeleteServlet\" method=\"delete\">"
					+ "</form>"
					+"</td>"*/
					+"</tr>\r\n"					
					);
		}
		out.println("<tr>"
				+ "<td></td>"
				+ "<td></td>"
				+ "<td></td>"
				+ "<td></td>"
				+ "<td>"
				+ "<form  action=\"UpdatePatient.html\" method=\"post\">"
				+ "<button id=\"update\" name=\"update\" class=\"btn btn-primary btn-block\" type=\"submit\" value=\"update Patient\">update Patient</button>"
				+ "</form>"
				+ "</td>"
				+ "<td></td>"
				+ "<td></td>"
				+ "<td></td>"
				+ "</tr>");
		out.println("</tbody></table>"
				+ "</div></section>"				
				+ "</body>"
				+ "</html>");
		}else if (type.equals("delete Patient")) {
			out.println("<title>Patients Search By ID</title>"
					+ "</head>"
					+ "<body>"
					+ "<header><h2>Patient ID:</h2></header>"
					+ "<section><div class=\"jumbotron\">"
					+ "<form action=\"PatientDeleteServlet\" method=\"post\">"
					+ "<table>"
					+ "<tr>"
					+ "<td>"
					+ "ID:"
					+ "</td>"
					+ "<td>"
					+ "<input  type=\"text\" class=\"form-control\" name=\"PatientID\" value=\"1\" />"
					+ "</td>"
					+ "</tr>"		
					+ "<tr>"
					+ "<td></td>"
					+ "<td>"
					+ "<button id=\"DeletePatient\" class=\"btn btn-primary btn-block\" type=\"submit\" value=\"DeletePatient\">Delete Patient</button>"
					+ "</td>"
					+ "</table>"
					+ "</form>"
					+ "</div></section>"	
					+ "</body>"
					+ "</html>");
		}else if (type.equals("create Patient")) {
			
			out.println("<p>Page will redirect to Creating page in following 2 sec</p>"
					+ "<tr><td><a href=\"CreatePatient.html\">Enter to creating page</a></td></tr>");
			response.setHeader("refresh","2;URL=CreatePatient.html");
		}
			
				
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
	}

}
