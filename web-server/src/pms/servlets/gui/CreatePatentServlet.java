package pms.servlets.gui;

	import java.io.IOException;
	import java.io.PrintWriter;
	import java.sql.DriverManager;
	import java.sql.ResultSet;
	import java.sql.SQLException;

	import javax.servlet.ServletException;
	import javax.servlet.annotation.WebServlet;
	import javax.servlet.http.HttpServlet;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;

import ie.ait.eng.sw.msc.agile.dao.PatientsDAO;
import ie.ait.eng.sw.msc.agile.util.Patients;
	@WebServlet(name = "CreatePatentServlet", urlPatterns = { "/CreatePatentServlet" })
	public class CreatePatentServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		@Override
		public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		}

		@Override
		public void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
		

			try {
				     
		            String firstname = request.getParameter("firstname"); 
		            String surname = request.getParameter("surname"); 
		            String gender = request.getParameter("gender"); 
		            String address = request.getParameter("address"); 
		            String telephone = request.getParameter("telephone"); 
		            String email = request.getParameter("email"); 		     
		            Patients patients = new Patients(firstname,surname,gender,address,telephone,email);
		            PatientsDAO DAO= new PatientsDAO();
		            DAO.add(patients);	
				
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				out.println("<table><tr><td>patient inserted successfully!</td></tr>");
		} catch (SQLException se) {
				// Handle errors for JDBC
				se.printStackTrace();
			} catch (Exception e) {
				// Handle errors for Class.forName
				e.printStackTrace();
			}
		}

	}
