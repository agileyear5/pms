package pms.servlets.gui;



import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;

import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Connection;
import java.sql.Statement;

@WebServlet(name ="PharmacistServlet", urlPatterns = { "/PharmacistServlet" })
 public class PharmacistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private String url;
	private String driver = null;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection connection = null;
		Statement statement = null;

		url = "jdbc:mysql://localhost:3306/PMS?user=admin&password=admin";
		driver = "com.mysql.jdbc.Driver";
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url);
		} catch (ClassNotFoundException | SQLException ex) {
			System.out.println("Error: unable to load driver class!");
			System.exit(1);
		}

		try {

			int ID = Integer.parseInt(request.getParameter("pid"));
			System.out.println("id = " + ID);
			String prescription = request.getParameter("prescription");
			String allergies = request.getParameter("allergies");
			String advice_info = request.getParameter("advice_info");

			String updateSql = "update pms.prescriptions set prescription='" + prescription + "', allergies='" + allergies
					+ "', advice_info='" + advice_info + "' where patientid='" + ID + "' ";

			System.out.println("updateSql = " + updateSql);
			statement = connection.createStatement();
			int result = statement.executeUpdate(updateSql);
			if (result != 0) {
				System.out.println("prescription Information Updated Successfully.......");
			} else {
				System.out.println("Nothing Updated .......");
			}
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<table><tr><td>Update prescription information successfully!</td></tr>");

			statement.close();
			connection.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}
	}

}
