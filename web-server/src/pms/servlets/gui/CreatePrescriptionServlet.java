package pms.servlets.gui;

	import java.io.IOException;
	import java.io.PrintWriter;
	import java.sql.DriverManager;
	import java.sql.ResultSet;
	import java.sql.SQLException;

	import javax.servlet.ServletException;
	import javax.servlet.annotation.WebServlet;
	import javax.servlet.http.HttpServlet;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;

	import ie.ait.eng.sw.msc.agile.util.Staff;

	import java.sql.Connection;
	import java.sql.Statement;
	@WebServlet(name = "CreatePrescriptionServlet", urlPatterns = { "/CreatePrescriptionServlet" })
	public class CreatePrescriptionServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		private String url;
		private String driver = null;

		@Override
		public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		}

		@Override
		public void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			Connection connection = null;
			Statement statement = null;

			url = "jdbc:mysql://localhost:3306/PMS?user=admin&password=admin";
			driver = "com.mysql.jdbc.Driver";
			try {
				Class.forName(driver);
				connection = DriverManager.getConnection(url);
			} catch (ClassNotFoundException | SQLException ex) {
				System.out.println("Error: unable to load driver class!");
				System.exit(1);
			}

			try {
				//int id = Integer.parseInt(request.getParameter("id"));
				int pid = Integer.parseInt(request.getParameter("pid"));
				System.out.println("id = " + pid);
				String prescription = request.getParameter("pres");
				String allergies = request.getParameter("all");
				String advice_info = request.getParameter("adv");

				String insert = "INSERT INTO pms.prescriptions (PATIENTID, PRESCRIPTION,ALLERGIES,ADVICE_INFO) VALUES ('"+pid+"', '"+prescription+"', '"+allergies+"', '"+advice_info+"')";

				System.out.println("insert = " + insert);
				statement = connection.createStatement();
				int result = statement.executeUpdate(insert);
				if (result != 0) {
					System.out.println("prescription Information insert Successfully.......");
				} else {
					System.out.println("Nothing Added .......");
				}
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				out.println("<table><tr><td>Add prescription information successfully!</td></tr>");

				statement.close();
				connection.close();
			} catch (SQLException se) {
				// Handle errors for JDBC
				se.printStackTrace();
			} catch (Exception e) {
				// Handle errors for Class.forName
				e.printStackTrace();
			}
		}

	}
