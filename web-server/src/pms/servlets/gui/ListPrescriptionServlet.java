/*------------------------------------------------------------------------------
 *******************************************************************************
 * COPYRIGHT Ericsson 2012
 *
 * The copyright to the computer program(s) herein is the property of
 * Ericsson Inc. The programs may be used and/or copied only with written
 * permission from Ericsson Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the
 * program(s) have been supplied.
 *******************************************************************************
 *----------------------------------------------------------------------------*/
package pms.servlets.gui;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import ie.ait.eng.sw.msc.agile.dao.impl.PrescriptionManagementDAOImpl;
import ie.ait.eng.sw.msc.agile.util.Prescription;


@WebServlet(name = "ListPrescriptionServlet", urlPatterns = { "/ListPrescriptionServlet" })
public class ListPrescriptionServlet extends HttpServlet {

	private String prescriptionId;
	private int pid;
	
	private String message;
	private static final long serialVersionUID = 1L;
	private String patientName;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		prescriptionId = request.getParameter("PrescriptionID");
		pid = Integer.valueOf(prescriptionId);
		
		PrescriptionManagementDAOImpl dao = new PrescriptionManagementDAOImpl();
		
		if (dao.isDbConnected()) {		
			try {			
				Prescription prescription = dao.getPrescription(pid);
				if (prescription.getId() > 0 ) {
					System.out.println("Patient ID returned is - " + prescription.getPatientId());
					message = "Prescription found: " + prescription.getPrescription() + "ID is: " + prescription.getPatientId();
					patientName = dao.getPatientName(prescription.getPatientId());
					writeOutputPrescription(response, prescription);

				} else {
					message = "No Prescription information found for ID " + pid;
					writeOutputMessage(response, message);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		} else {
			message = "Cannot connect to the database";
			writeOutputMessage(response, message);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}
	
	private void writeOutputMessage (HttpServletResponse response, String msg) throws IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html><body>");
		out.println(message);
		out.println("<br><a href=\"PharmacistPage.html\">Return</a>");
		out.println("</body></html>");
	}

	private void writeOutputPrescription (HttpServletResponse response, Prescription prescription) throws IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html><body>");

		out.println("<script type=\"text/javascript\">");
		out.println("function checkInputParameters() {");
		out.println("var prescription_details = document.getElementById(\"Prescription\").value;");
		
		out.println("if ( prescription_details.length === 0 ) {");		
		out.println("alert(\"Prescription input cannot be empty!\")");
		out.println("return false;");
		out.println("} else {");
		out.println("return true;");
		out.println("}}</script>");

		out.println("<H1 align=\"center\">Update Prescription</H1>");
		out.println("<form action=\"UpdatePrescriptionServlet\" method=\"post\">");
		out.println("<table border=\"1\" align=\"center\">");
		out.println("<tr><td align=\"center\">Prescription ID:</td><td align=\"center\"><input type=\"text\" readonly=\"true\" name=\"PrescriptionID\" id=\"PrescriptionID\" value=\"" + prescriptionId + "\"></td></tr>");
		out.println("<tr><td align=\"center\">Patient Name:</td><td align=\"center\"><input type=\"text\" readonly=\"true\" name=\"PatientID\" id=\"PatientID\" value=\"" +  patientName + "\"></td></tr>");
		out.println("<tr><td align=\"center\">Prescription Details:</td><td align=\"center\"><input type=\"text\" name=\"Prescription\" id=\"Prescription\" value=\"" + prescription.getPrescription() + "\"></td></tr>");
		out.println("<tr><td align=\"center\">Allergies:</td><td align=\"center\"><input type=\"text\" name=\"Allergies\" id=\"Allergies\" value=\"" + prescription.getAllergies() + "\"></td></tr>");
		out.println("<tr><td align=\"center\">Advice Information:</td><td align=\"center\"><textarea rows=\"3\" cols=\"20\" name=\"Advice_Info\" id=\"Advice_Info\">" + prescription.getAdvice_info() + "</textarea></td></tr>");
		out.println("<tr><td align=\"center\">Comments:</td><td><textarea rows=\"3\" cols=\"20\"  name=\"Comments\" id=\"Comments\">" + prescription.getComments() + "</textarea></td></tr>");
		out.println("<tr><td colspan=\"2\" align=\"center\"><input type=\"submit\" value=\"Update\" onclick=\"return checkInputParameters();\"></td>" + "</textarea></td></tr>");
		out.println("</table>");
		out.println("<p align=\"center\"><a href=\"PharmacistPage.html\">Return</a></p>");
		out.println("</body></html>");

	}

}
