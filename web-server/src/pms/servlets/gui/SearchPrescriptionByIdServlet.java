package pms.servlets.gui;

import java.io.IOException;

import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ie.ait.eng.sw.msc.agile.dao.PrescriptionDAO;

import ie.ait.eng.sw.msc.agile.util.Prescription;

import javax.servlet.annotation.WebServlet;

@WebServlet(name = "SearchPrescriptionByIdServlet", urlPatterns = { "/SearchPrescriptionByIdServlet" })
public class SearchPrescriptionByIdServlet extends HttpServlet {

	PrescriptionDAO PRDAO = null;
	Prescription prescription = null;
	PrintWriter out = null;
	private static final long serialVersionUID = 1L;

	public void WritePage(int patientId, PrintWriter out, HttpServletResponse response) {

		try {
			if (patientId == 0) {
				response.setContentType("text/html");

				response.setContentType("text/html");
				out.println("there is no user with 0 ID" + "<FORM ACTION=\"GP.html\" METHOD=\"POST\">"
						+ "<input type=\"submit\" name=\"submit\" value=\"Search\">" + "</FORM>");

			} else {
				PRDAO = new PrescriptionDAO();
				prescription = PRDAO.search(patientId);
				response.setContentType("text/html");
				out.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">");
				out.println("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>>");
				out.println("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>");
				out.println(
						"<table class=\"table\"><tr><td>Patient Emergency ID</td><td>PRESCRIPTION</td><td>ALLERGIES</td><td>ADVICE_INFO</td><td>COMMENTS</td></tr>");
				// out.println("<FORM ACTION=\"PharmacistServlet\"
				// METHOD=\"POST\" id =\"" + patientId + "\">");
				out.println("<tr><td>" + /*"<input type=\"text\" name=\"id\" id=\"id\" value=\"" + prescription.getId()
						+ "\" readonly>" + "</td><td>" +*/ "<input type=\"text\" name=\"pid\" id=\"pid\" value=\""
						+ patientId + "\" readonly>"+ "</td><td>"
						+ "<input class=\"form-control\" type=\"text\" name=\"prescription\" id=\"prescription\" value=\""
						+ prescription.getPrescription() + "\">" + "</td><td>"
						+ "<input class=\"form-control\" type=\"text\" name=\"allergies\" id=\"allergies\" value=\""
						+ prescription.getAllergies() + "\">" + "</td><td>"
						+ "<input class=\"form-control\" type=\"text\" name=\"advice_info\" id=\"advice_info\" value=\""
						+ prescription.getAdvice_info() + "\">" + "</td><td>"
						+ "<input class=\"form-control\" type=\"text\" name=\"advice_info\" id=\"advice_info\" value=\""
						+ prescription.getComments() + "\">" + "</td></tr>");
				// out.println("</FORM>");
				out.println("</table>");

			}
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, NumberFormatException, NumberFormatException {
		// String role = request.getParameter("role");
		int ID = Integer.parseInt(request.getParameter("pid"));
		out = response.getWriter();
		WritePage(ID, out, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);

	}

}
