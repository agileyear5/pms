package pms.servlets.gui; 

  

import java.io.IOException; 


import java.io.PrintWriter; 

import java.sql.DriverManager; 

import java.sql.ResultSet; 

import java.sql.SQLException; 

  

import javax.servlet.ServletException; 

import javax.servlet.annotation.WebServlet; 

import javax.servlet.http.HttpServlet; 

import javax.servlet.http.HttpServletRequest; 

import javax.servlet.http.HttpServletResponse; 

  

import ie.ait.eng.sw.msc.agile.dao.StaffDAO; 

import ie.ait.eng.sw.msc.agile.util.Staff; 

  

import java.sql.Connection; 

import java.sql.Statement; 

  

@WebServlet(name = "/UpdateServlet", urlPatterns = { "/UpdateServlet" }) 

public class UpdateServlet extends HttpServlet { 

    private static final long serialVersionUID = 1L; 

  

    /* 

     * private String url; private String driver = null; 

     */ 

  

    @Override 

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 

  

    } 

  

    @Override 

    public void doPost(HttpServletRequest request, HttpServletResponse response) 

            throws ServletException, IOException { 

        /* 

         * Connection connection = null; Statement statement = null; 

         */ 

  

        /* 

         * url = "jdbc:mysql://localhost:3306/PMS?user=admin&password=admin"; 

         * driver = "com.mysql.jdbc.Driver"; try { Class.forName(driver); 

         * connection = DriverManager.getConnection(url); } catch 

         * (ClassNotFoundException | SQLException ex) { 

         * System.out.println("Error: unable to load driver class!"); 

         * System.exit(1); } 

         */ 

        // int id = Integer.parseInt(request.getParameter("id")); 

  

        try { 

  

            int id = Integer.parseInt(request.getParameter("id")); 

            String firstname = request.getParameter("firstname"); 

            String surname = request.getParameter("surname"); 

            String password = request.getParameter("password"); 
            
            String address = request.getParameter("address");
            
            String telephone = request.getParameter("telephone");
            
            String email = request.getParameter("email");

            String role = request.getParameter("role"); 

  

            Staff staff = new Staff(id, firstname, surname, password, address, telephone, email, role); 

            StaffDAO DAO = new StaffDAO(); 

            DAO.update(staff); 

  

            /* 

             * System.out.println("id = " + id); String firstname 

             * =request.getParameter("firstname"); String surname 

             * =request.getParameter("surname"); String password 

             * =request.getParameter("password"); String role 

             * =request.getParameter("role"); 

             *  

             * String updateSql = "update pms.staff set firstname='" + firstname 

             * + "', surname='" + surname + "', password='" + password + 

             * "', role='" + role + "' where ID='" + id + "' "; 

             *  

             * System.out.println("updateSql = " + updateSql); statement = 

             * connection.createStatement(); int result = 

             * statement.executeUpdate(updateSql); if (result != 0) { 

             * System.out. 

             * println("Staff Information Updated Successfully......."); } else 

             * { System.out.println("Nothing Updated ......."); } 

             */ 

            response.setContentType("text/html"); 

            PrintWriter out = response.getWriter(); 

            out.println("<table><tr><td>Update staff information successfully!</td></tr>"); 
            out.println("<tr><td><a href=\"Admin.html\">Return Main Menu</a></td></tr>");

            out.println("</table>"); 

  

            /* 

             * statement.close(); connection.close(); 

             */ 

        } /* 

             * catch (SQLException se) { // Handle errors for JDBC 

             * se.printStackTrace(); } 

             */catch (Exception e) { 

            // Handle errors for Class.forName 

            e.printStackTrace(); 

        } 

    } 

  

} 