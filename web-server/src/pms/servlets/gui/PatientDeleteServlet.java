package pms.servlets.gui;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ie.ait.eng.sw.msc.agile.dao.PatientsDAO;

/**
 * Servlet implementation class PatientDeleteServlet
 */
@WebServlet(
		name = "/PatientDeleteServlet",
		urlPatterns = { "/PatientDeleteServlet" })
public class PatientDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PatientDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		PrintWriter out = response.getWriter();
		if (request.getParameter("PatientID")!= null) {
			int id = Integer.parseInt(request.getParameter("PatientID"));
			try {
				PatientsDAO pdao = new PatientsDAO();
				if (pdao.DeletePatient(id))								
					out.println("Patient No: " + id +" has been deleted!");
				else
					out.println("Patient No: " + id +" is not exit!");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
		}
		else
			out.println("Please type in a number!");
					
		
	}

}
