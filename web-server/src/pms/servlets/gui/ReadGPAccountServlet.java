package pms.servlets.gui;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "ReadGPAccountServlet", urlPatterns = { "/ReadGPAccountServlet" })
public class ReadGPAccountServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private String url;
	private String driver = null;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Connection connection = null;
		url = "jdbc:mysql://localhost:3306/PMS?user=admin&password=admin";
		driver = "com.mysql.jdbc.Driver";
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url);
		} catch (ClassNotFoundException | SQLException ex) {
			System.out.println("Error: unable to load driver class!");
			System.exit(1);
		}

		try {
			
			String sql = "SELECT Id,FirstName,surname,role FROM pms.staff;";
			Statement statement = connection.createStatement();
			ResultSet resultset = statement.executeQuery(sql);
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<table><tr><td>ID</td><td>Firstname</td></tr>");
			while (resultset.next()) {
			    String id = resultset.getString("id");
			    String firstname = resultset.getString("firstname"); 
			    String surname = resultset.getString("surname");
			    String address = resultset.getString("address");
			    String tel = resultset.getString("tel");
			    String email = resultset.getString("email");

				
				out.println("<FORM ACTION=\"ReadGPAccountServlet\" METHOD=\"POST\" id =\""+id+"\">");
				out.println("<tr><td>"+ "<input type=\"text\" name=\"id\" id=\"id\" value=\"" + id + "\" readonly>" 
						+ "</td><td>" + "<input type=\"text\" name=\"firstname\" id=\"firstname\" value=\"" + firstname+"</td><td>" + "<input type=\"text\" name=\"surname\" id=\"surname\" value=\"" + surname + "\"></td>"+"<td>" + "<input type=\"text\" name=\"address\" id=\"address\" value=\""+address+"</td><td>" + "<input type=\"text\" name=\"tel\" id=\"tel\" value=\"" +tel+"</td><td>" + "<input type=\"text\" name=\"email\" id=\"email\" value=\"" +email+"</td></tr>" );

				out.println("</FORM>");
				

				
			}
			out.println("</table>");
			out.println("<tr><td><a href=\"Admin.html\">Return Main Menu</a></td></tr>");
			resultset.close();
			statement.close();
			connection.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}
	} // end try

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
