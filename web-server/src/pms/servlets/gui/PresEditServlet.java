package pms.servlets.gui;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ie.ait.eng.sw.msc.agile.dao.PrescriptionDAO;
import ie.ait.eng.sw.msc.agile.util.Prescription;

/**
 * Servlet implementation class PresEditServlet
 */
@WebServlet(name="/PresEditServlet" ,urlPatterns = {"/PresEditServlet"})
public class PresEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String prescriptionDetails;
	private String allergies;
	private String adviceInformation;
	private String comments;
	PrescriptionDAO PRDAO = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PresEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String deleteId = request.getParameter("delete");
		String editId = request.getParameter("edit");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		if(deleteId==null) {
			int eId = Integer.parseInt(editId);
			
			//int ppid = Integer.parseInt(pId);
			prescriptionDetails = request.getParameter("PresDetail"+editId);
			allergies = request.getParameter("Allergies"+editId);
			adviceInformation = request.getParameter("Advice_info"+editId);
			comments = request.getParameter("Comments"+editId);
			
			out.println(prescriptionDetails);
			out.println(allergies);
			out.println(comments);
			out.println(adviceInformation);
			
			Prescription pres = new Prescription();
			pres.setAdvice_info(adviceInformation);
			
			pres.setComments(comments);
			pres.setAllergies(allergies);
			pres.setPrescription(prescriptionDetails);
			pres.setId(eId);
			try {
				boolean updateStatus = PRDAO.updatePres(pres);
				if (updateStatus)
					out.println("prescription: "+editId+" has been updated");
				else
					out.println("prescription: "+editId+" update fail");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else {
			int dId = Integer.parseInt(deleteId);
			try {
				boolean deleteStatus = PRDAO.deletePres(dId);
				if (deleteStatus)
					out.println("prescription: "+editId+" has been delete");
				else
					out.println("prescription: "+editId+" delete fail");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
