package pms.servlets.gui;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ie.ait.eng.sw.msc.agile.dao.impl.PrescriptionManagementDAOImpl;

/**
 * Servlet implementation class StaffLogin
 */
@WebServlet(name="/StaffLoginServlet",urlPatterns = {"/StaffLoginServlet"})
public class StaffLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PrescriptionManagementDAOImpl dao = new PrescriptionManagementDAOImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StaffLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    private void WritePage(String role,PrintWriter out, HttpServletResponse response) {
    	switch(role) {
    		case "GP":
    			out.println("<p>login success, page will redirect to gp page in following 2 sec</p>"
    					+ "<tr><td><a href=\"GP.html\">Enter GP page</a></td></tr>");
    			response.setHeader("refresh","2;URL=GP.html");
    			break;

    		case "Admin":
    			out.println("<p>login success, page will redirect to Admin page in following 2 sec</p>"
    					+ "<br><tr><td><a href=\"Admin.html\">Enter Admin page</a></td></tr></br>");
    			response.setHeader("refresh","2;URL=Admin.html");
    			break;

			case "Pharmacist":
				out.println("<p>login success, page will redirect to PharmacistPage page in following 2 sec</p>"
						+ "<br><tr><td><a href=\"PharmacistPage.html\">Enter Pharmacist page</a></td></tr></br>");
				response.setHeader("refresh","2;URL=PharmacistPage.html");
				break;
				
			case "Other":
				out.println("<p>login success, page will redirect to PharmacistPage page in following 2 sec</p>"
						+ "<br><tr><td><a href=\"OtherStaff.html\">Enter OtherStaff page</a></td></tr></br>");
				response.setHeader("refresh","2;URL=OtherStaff.html");
				break;
    	}
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String UserName = request.getParameter("UserName");
		String SurName = request.getParameter("SurName");
		String Password = request.getParameter("Password");
		
		String role = request.getParameter("role");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		//out.println("User Name is:" +UserName+", Sur name is: "+SurName+", Password is: " +Password);
		if (UserName == null || SurName ==null || Password == null)
			out.println("UserName or SurName or Password is null, pls try again");
		else 
			if (dao.StaffLogin(UserName, SurName, Password, role)){
				WritePage(role,out,response);
			}
			else 
				out.println("UserName or SurName or Password or role is not match, pls try again");
		
	}

}
