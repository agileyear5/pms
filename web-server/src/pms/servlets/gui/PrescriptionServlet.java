package pms.servlets.gui;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ie.ait.eng.sw.msc.agile.dao.PrescriptionDAO;
import ie.ait.eng.sw.msc.agile.util.Prescription;

/**
 * Servlet implementation class PrescriptionServlet
 */
@WebServlet(name="/PrescriptionServlet" ,urlPatterns = {"/PrescriptionServlet"})
public class PrescriptionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	PrescriptionDAO PRDAO = null;
	Prescription pres = null;
	List<Prescription> presList = new ArrayList<>();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PrescriptionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String type = request.getParameter("PresOperation");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html>");    
        out.println("<head>");   
        out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"style/bootstrapV4.css\"/>\r\n" + 
        		"<link rel=\"stylesheet\" type=\"text/css\" href=\"style/font-awesome.css\"/>\r\n" + 
        		"<link rel=\"stylesheet\" type=\"text/css\" href=\"style/screen.css\"/>\r\n" + 
        		"<link rel=\"stylesheet\" href=\"rstyle/jquery.dataTables.css\">\r\n" + 
        		"<script type=\"text/javascript\" src=\"style/jquery.js\"></script>\r\n" + 
        		"<script type=\"text/javascript\" src=\"style/popper.min.js\"></script>\r\n" + 
        		"<script type=\"text/javascript\" src=\"style/jquery.dataTables.js\"></script>\r\n" + 
        		"<script type=\"text/javascript\" src=\"style/bootstrapV4.js\"></script>");

		if (type.equals("Search")) {
			if (request.getParameter("pid")==null) {
		        out.println("pls enter correct id");
			}else {
				String id =request.getParameter("pid");
				int Pid = Integer.parseInt(id);
				try {
					PRDAO = new PrescriptionDAO();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				pres = PRDAO.search(Pid);
				out.println("<title>All Patients List</title>");    
		        //out.println("<link href=\"style/GP.css\" rel=\"stylesheet\" type=\"text/css\">");        
		        out.println("</head>");    
		        out.println("<body>");    
				out.println("<header><h2>Prescription list</h2></header>\r\n" + 
						"<section><div class=\"jumbotron\">"+
						"<form action=\"PresEditServlet\" method=\"post\" value="+pres.getId()+">"+
						"<table class=\"table table-hover center-block\">\r\n" + 
						"<thead>\r\n" + 
						"<th>ID</th>\r\n" + 
						"<th>Patient ID</th>\r\n" + 
						"<th>Prescription</th>\r\n" +
						"<th>ALLERGIES</th>\r\n" +
						"<th>Advice info</th>\r\n" +
						"<th>Comments</th>\r\n" +
						"</thead><tbody>");
				out.println(
						"<tr>\r\n" +
						"<td>"+pres.getId() +"</td>\r\n" + 
						"<td><input type=\"number\" class=\"form-control\" name=\"patientId"+pres.getId() +"\" value="+pres.getPatientId()+" disable></td>\r\n"+ 
						"<td><input type=\"text\" class=\"form-control\" name=\"PresDetail"+pres.getId() +"\" value="+pres.getPrescription()+"></td>\r\n"+ 
						"<td><input type=\"text\" class=\"form-control\" name=\"Allergies"+pres.getId() +"\" value="+pres.getAllergies()+"></td>\r\n"+ 
						"<td><input type=\"text\" class=\"form-control\" name=\"Advice_info"+pres.getId() +"\" value="+pres.getAdvice_info()+"></td>\r\n"+ 
						"<td><input type=\"text\" class=\"form-control\" name=\"Comments"+pres.getId() +"\" value="+pres.getComments()+"></td>\r\n"+ 
						"<td>"
						+ "<button id=\"delete\" name=\"delete\" class=\"btn btn-sm btn-primary btn-block\" type=\"submit\" value="+pres.getId()+">delete</button>"
						+ "</td><td>"
						+ "<button id=\"delete\" name=\"edit\" class=\"btn btn-sm btn-primary btn-block\" type=\"submit\" value="+pres.getId()+">edit</button>"
						+ "</td>"
						/*+"<td>"
						+ "<form action=\"PatientUpdateServlet\" method=\"post\" value="+id+">"
						+ "<input type=\"submit\" name=\"update\" id=\"update\" value =\"update Patient\">"
						+ "<form action=\"PatientDeleteServlet\" method=\"delete\">"
						+ "</form>"
						+"</td>"*/
						+"</tr>\r\n"	
						
						);
			}
		}else if(type.equals("AllPre")) {
			try {
				presList = PRDAO.getAll();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			out.println("<title>All Prescription List</title>");            
	        out.println("</head>");    
	        out.println("<body>");    
			out.println("<header><h2>Prescription list</h2></header>\r\n" + 
					"<section><div class=\"jumbotron\">"+
					"<form action=\"PresEditServlet\" method=\"post\" >"+
					"<table class=\"table table-hover center-block\">\r\n" + 
					"<thead>\r\n" + 
					"<th>ID</th>\r\n" + 
					"<th>Patient ID</th>\r\n" + 
					"<th>Prescription</th>\r\n" +
					"<th>ALLERGIES</th>\r\n" +
					"<th>Advice info</th>\r\n" +
					"<th>Comments</th>\r\n" +
					"</thead><tbody>");
			for(int i = 0 ; i < presList.size() ; i++) {
				out.println(
						
						"<tr>\r\n" +
						"<td>"+presList.get(i).getId() +"</td>\r\n" + 
						"<td><input type=\"number\" class=\"form-control\" name=\"patientId"+presList.get(i).getPatientId() +"\" value="+presList.get(i).getPatientId()+" disable></td>\r\n"+  
						"<td><input type=\"text\" class=\"form-control\" name=\"PresDetail"+presList.get(i).getId()+"\" value="+presList.get(i).getPrescription()+"></td>\r\n"+ 
						"<td><input type=\"text\" class=\"form-control\" name=\"Allergies"+presList.get(i).getId()+"\" value="+presList.get(i).getAllergies()+"></td>\r\n"+ 
						"<td><input type=\"text\" class=\"form-control\" name=\"Advice_info"+presList.get(i).getId()+"\" value="+presList.get(i).getAdvice_info()+"></td>\r\n"+ 
						"<td><input type=\"text\" class=\"form-control\" name=\"Comments"+presList.get(i).getId()+"\" value="+presList.get(i).getComments()+"></td>\r\n"+ 
						"<td>"
						+ "<button id=\"delete\" name=\"delete\" class=\"btn btn-sm btn-primary btn-block\" type=\"submit\" value="+presList.get(i).getId()+">delete</button>"
						+ "</td><td>"
						+ "<button id=\"delete\" name=\"edit\" class=\"btn btn-sm btn-primary btn-block\" type=\"submit\" value="+presList.get(i).getId()+">edit</button>"
						+ "</td>"						
						+"</tr>\r\n"
						+ "</form>"
						);
			}
		}
		out.println("</tbody></table>"
				+ "</form>"
				+ "</div></section>"				
				+ "</body>"
				+ "</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
