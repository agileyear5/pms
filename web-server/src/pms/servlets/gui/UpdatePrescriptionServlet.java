/*------------------------------------------------------------------------------
 *******************************************************************************
 * COPYRIGHT Ericsson 2012
 *
 * The copyright to the computer program(s) herein is the property of
 * Ericsson Inc. The programs may be used and/or copied only with written
 * permission from Ericsson Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the
 * program(s) have been supplied.
 *******************************************************************************
 *----------------------------------------------------------------------------*/
package pms.servlets.gui;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import ie.ait.eng.sw.msc.agile.dao.impl.PrescriptionManagementDAOImpl;
import ie.ait.eng.sw.msc.agile.util.Prescription;


@WebServlet(name = "UpdatePrescriptionServlet", urlPatterns = { "/UpdatePrescriptionServlet" })
public class UpdatePrescriptionServlet extends HttpServlet {

	private String prescriptionId;
	private int pid;
	private String prescriptionDetails;
	private String allergies;
	private String adviceInformation;
	private String comments;
	
	private String message;
	
	private static final int DUMMY_PATIENT_ID = 0;
	private static final int DB_UPDATED = 1;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		prescriptionId = request.getParameter("PrescriptionID");
		pid = Integer.valueOf(prescriptionId);
		
		prescriptionDetails = request.getParameter("Prescription");
		allergies = request.getParameter("Allergies");
		adviceInformation = request.getParameter("Advice_Info");
		comments = request.getParameter("Comments");
		
		//message = prescriptionId + " - " + prescriptionDetails + " - " + allergies + " - " + adviceInformation + " - " + comments;
		Prescription prescription = new Prescription(pid, DUMMY_PATIENT_ID, prescriptionDetails, allergies, adviceInformation, comments);
		PrescriptionManagementDAOImpl dao = new PrescriptionManagementDAOImpl();
		
		if (dao.isDbConnected()) {		
			try {
				if ( dao.updatePrescription(prescription) == DB_UPDATED) {
					message = "Prescription updated successfully";
				} else {
					message = "Failed to update prescription";
				}
					writeOutputMessage(response, message);
			} catch (SQLException e) {
				e.printStackTrace();
			}

		} else {
			message = "Cannot connect to the database";
			writeOutputMessage(response, message);
		}
	}
	
	private void writeOutputMessage (HttpServletResponse response, String msg) throws IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html><body>");
		out.println("<H1 align=\"center\">Result of update</H1>");
		out.println("<p align=\"center\"><font color=\"red\">" + message + "</p>");
		out.println("<p align=\"center\"><a href=\"PharmacistPage.html\">Return</a></p>");
		out.println("</body></html>");

	}

}
