package pms.servlets.gui;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DeletePrescriptionServlet
 */
@WebServlet(name = "DeletePrescriptionServlet", urlPatterns = { "/DeletePrescriptionServlet" })
public class DeletePrescriptionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String url;
	private String driver = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeletePrescriptionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection connection = null;
		Statement statement = null;

		url = "jdbc:mysql://localhost:3306/PMS?user=admin&password=admin";
		driver = "com.mysql.jdbc.Driver";
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url);
		} catch (ClassNotFoundException | SQLException ex) {
			System.out.println("Error: unable to load driver class!");
			System.exit(1);
		}

		try {
			int pid = Integer.parseInt(request.getParameter("pid"));
			System.out.println("id = " + pid);	

			String delete ="DELETE FROM pms.prescriptions WHERE PATIENTID='"+pid+"'" ;

			System.out.println("delete = " + delete);
			statement = connection.createStatement();
		    int result = statement.executeUpdate(delete);
			if (result != 0) {
				System.out.println("prescription Information delete Successfully.......");
			} else {
				System.out.println("Nothing deleted .......");
			}
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<table><tr><td>Delete prescription information successfully!</td></tr>");

			statement.close();
			connection.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}
	}

	}


