package pms.servlets.gui;


	import java.io.IOException;
	import java.io.PrintWriter;
	import java.sql.DriverManager;
	import java.sql.ResultSet;
	import java.sql.SQLException;

	import javax.servlet.ServletException;
	import javax.servlet.annotation.WebServlet;
	import javax.servlet.http.HttpServlet;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;

	import ie.ait.eng.sw.msc.agile.util.Staff;

	import java.sql.Connection;
	import java.sql.Statement;
	@WebServlet(name = "DeleteGP", urlPatterns = { "/DeleteGP" })
	public class DeleteGP extends HttpServlet {
		private static final long serialVersionUID = 1L;

		private String url;
		private String driver = null;

		@Override
		public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		}

		@Override
		public void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			Connection connection = null;
			Statement statement = null;
			// ResultSet resultset=null;

			url = "jdbc:mysql://localhost:3306/PMS?user=admin&password=admin";
			driver = "com.mysql.jdbc.Driver";
			try {
				Class.forName(driver);
				connection = DriverManager.getConnection(url);
			} catch (ClassNotFoundException | SQLException ex) {
				System.out.println("Error: unable to load driver class!");
				System.exit(1);
			}

			try {

				String id = request.getParameter("id");		
				
				String insert = "Delete from pms.staff where id="+id+"AND role='GP'";
				statement = connection.createStatement();
				statement.executeUpdate(insert);
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				out.println("<table><tr><td>Delete GP  successfully!</td></tr>");
				out.println("<tr><td><a href=\"Admin.html\">Return Main Menu</a></td></tr>");
				statement.close();
				connection.close();
			} catch (SQLException se) {
				// Handle errors for JDBC
				se.printStackTrace();
			} catch (Exception e) {
				// Handle errors for Class.forName
				e.printStackTrace();
			}
		}

	}