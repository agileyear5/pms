package pms.servlets.gui;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ie.ait.eng.sw.msc.agile.dao.Conn_db;

import ie.ait.eng.sw.msc.agile.dao.PatientsDAO;
import ie.ait.eng.sw.msc.agile.dao.PharDAO;
import ie.ait.eng.sw.msc.agile.dao.PrescriptionDAO;
import ie.ait.eng.sw.msc.agile.dao.StaffDAO;
import ie.ait.eng.sw.msc.agile.dao.impl.DbConnection;
import ie.ait.eng.sw.msc.agile.util.Patients;
import ie.ait.eng.sw.msc.agile.util.Prescription;
import ie.ait.eng.sw.msc.agile.util.Staff;
import ie.ait.eng.sw.msc.agile.util.pharmacist;

import javax.servlet.annotation.WebServlet;

@WebServlet(name = "PatientSearchServlet", urlPatterns = { "/PatientSearchServlet" })
public class PatientSearchServlet extends HttpServlet {
	
	Staff staff = null;
	StaffDAO DAO = null;
	PatientsDAO PDAO = null;
	Patients patient = null;
	pharmacist pharmacist = null;
	PharDAO PHDAO = null;
	PrescriptionDAO PRDAO = null;
	Prescription prescription = null;
	PrintWriter out = null;
	private static final long serialVersionUID = 1L;
	
	public void WritePage(String ID,PrintWriter out, HttpServletResponse response) {
	
	
	try {
	
	
		if  (ID == "") {
			response.setContentType("text/html");
			out.println("there is no user with 0 ID"+ "<FORM ACTION=\"Search.html\" METHOD=\"POST\">" +
				"<input type=\"submit\" name=\"submit\" value=\"Search\">"
						+ "</FORM>");
			
		}else
			PRDAO = new PrescriptionDAO();
			prescription = PRDAO.SearchEngine(ID);
			response.setContentType("text/html");
			out.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">");
			out.println("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>>");
			out.println("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>");
			out.println("<table class=\"table\" ><tr><td>ID</td><td>Patient ID</td><td>PRESCRIPTION</td><td>ALLERGIES</td><td>comments</td><td>ADVICE_INFO</td>");
			out.println("<tr><td>" + "<input  class=\"form-control\" type=\"text\" name=\"id\" id=\"id\" value=\"" + prescription.getId()+ "\" readonly>" + "</td><td>" 
					+ "<input class=\"form-control\"  type=\"text\" name=\"pid\" id=\"pid\" value=\"" + prescription.getPatientId() + "\" readonly>"
					+ "<input class=\"form-control\"  type=\"text\" name=\"prescription\" id=\"prescription\" value=\"" + prescription.getPrescription()	+ "\"readonly>" + "</td><td>" 
					+ "<input class=\"form-control\"  type=\"text\" name=\"allergies\" id=\"allergies\" value=\"" + prescription.getAllergies()	+ "\"readonly>" + "</td><td>"  
					+ "<input class=\"form-control\"  type=\"text\" name=\"allergies\" id=\"allergies\" value=\"" + prescription.getComments()	+ "\"readonly>" + "</td><td>" 
					+ "<input class=\"form-control\"  type=\"text\" name=\"advice_info\" id=\"advice_info\" value=\""	+ prescription.getAdvice_info() + "\"readonly>" + "</td></tr>"
					+ "<tr><td></td></tr>");
			out.println("</table>");
			
					
				
	
	} catch (Exception e) {

		e.printStackTrace();
	}
}
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, NumberFormatException,NumberFormatException {
		    String ID = request.getParameter("pid");
		    out = response.getWriter();
		    WritePage(ID,out,response);
	
	}
	
	

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
		
	
	}
	
}
