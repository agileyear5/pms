/*------------------------------------------------------------------------------
 *******************************************************************************
 * COPYRIGHT Ericsson 2012
 *
 * The copyright to the computer program(s) herein is the property of
 * Ericsson Inc. The programs may be used and/or copied only with written
 * permission from Ericsson Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the
 * program(s) have been supplied.
 *******************************************************************************
 *----------------------------------------------------------------------------*/
package pms.servlets.gui;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import ie.ait.eng.sw.msc.agile.dao.impl.PrescriptionManagementDAOImpl;

@WebServlet(name="LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {
	private String username; 
	private String password;
	private String loginMesage;
		
	private String dbSatus;
	private String role;

	private static final long serialVersionUID = 1L;

	@Override
	public void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		username = request.getParameter("Username");
		password = request.getParameter("Password");
		PrescriptionManagementDAOImpl dao = new PrescriptionManagementDAOImpl();
		
		if (dao.isDbConnected()) {		
			try {			
				role = dao.validateLogin(username, password);
				if (role == null) {
					loginMesage = "Invalid login";
					
					
				} else {
					loginMesage = "Welcome back " + username + " you are a " + role;
				
				
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			loginMesage = "Cannot connect to the database";
		}
		
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<html");
			out.println("<body>");
			out.println(loginMesage);
			if(role.equals("ADMIN")){

			out.println("<br><tr><td><a href=\"Admin.html\">If the page didn't change,Click Enter Admin page</a></td></tr></br>");
			response.setHeader("refresh","2;URL=Admin.html");

			}
			else if(role.equals("GP")){
			
				out.println("<br><tr><td><a href=\"GP.html\">If the page didn't change,Click enter GP page</a></td></tr></br>");
				response.setHeader("refresh","2;URL=GP.html");
				
			}
			else if(role.equals("PHARMACIST")){

				out.println("<br><tr><td><a href=\"PharmacistPage.html\">If the page didn't change,Click Enter Pharmacist page</a></td></tr></br>");
				response.setHeader("refresh","2;URL=PharmacistPage.html");
		
			}
			else if(role.equals("PATIENT")){ 

				out.println("<br><tr><td><a href=\"Patient.html\">If the page didn't change,Click Enter Pharmacist page</a></td></tr></br>");
				response.setHeader("refresh","2;URL=Patient.html");
			}
			else if(role.equals("STAFF")){ 

				out.println("<br><tr><td><a href=\"OtherStaff.html\">If the page didn't change,Click Enter OtherStaff page</a></td></tr></br>");
				response.setHeader("refresh","2;URL=OtherStaff.html");
			}
			 
			else{
				out.println("Sorry ,you can't see anything :( ....");
			}
			out.println("</body></html>");
			
			
			
	
	}

	public void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
}
