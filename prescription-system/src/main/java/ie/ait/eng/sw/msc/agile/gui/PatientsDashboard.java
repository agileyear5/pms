package ie.ait.eng.sw.msc.agile.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import ie.ait.eng.sw.msc.agile.dao.PatientsDAO;
import ie.ait.eng.sw.msc.agile.util.Patients;
import java.awt.BorderLayout;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import ie.ait.eng.sw.msc.agile.dao.Conn_db;
import javax.swing.JTable;
import java.awt.Button;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

public class PatientsDashboard {

	private JFrame frmPatient;
	private JTextField txtId;
	private JTextField txtFirstName;
	private JTextField txtSecondName;
	private JTextField txtId_1;
	private JTable table;;
	private JLabel msg;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PatientsDashboard window = new PatientsDashboard();
					window.frmPatient.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PatientsDashboard() {
		initialize();
		try {
			showTableData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPatient = new JFrame();
		frmPatient.setTitle("Patient");
		frmPatient.setVisible(true);
		frmPatient.setBounds(100, 100, 800, 600);
		frmPatient.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPatient.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 28, 131, 533);
		frmPatient.getContentPane().add(panel);
		panel.setLayout(new MigLayout("", "[]", "[][][][][][][][][][][][][][][]"));

		JButton btnNewButton_2 = new JButton("Search Patient");
		btnNewButton_2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				try {
					
					int ID = Integer.parseInt(txtId_1.getText());
					PatientsDAO myDAO = new PatientsDAO();
					
				//	Patients myPatient = myDAO.findPatient(ID);
					
					ResultSet myPatientRs = myDAO.findPatientsRS(ID);
					
					
//					String theDbUrl = "jdbc:mysql://localhost:3306/pmsstaff";
//					Statement stmt = Conn_db.connection(theDbUrl);
//					Patients patients = new Patients();
//					PatientsDAO pa = new PatientsDAO();
//					
//					pa.findPatient(ID);
					
					table.setModel(DbUtils.resultSetToTableModel(myPatientRs));
				
					
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		});

		JButton btnNewButton = new JButton("Create Patient");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

			}
		});
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				try {

					int patientId = Integer.parseInt(txtId.getText());
					String firstName = txtFirstName.getText();
					String secondName = txtSecondName.getText();
					Patients patients = new Patients(patientId, firstName, secondName);
					PatientsDAO pa = new PatientsDAO();
					pa.add(patients);
					msg.setText("Msg: create successful!");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					showTableData();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		});

		JButton btnNewButton_1 = new JButton("View Patients");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				try {
					showTableData();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		panel.add(btnNewButton_1, "cell 0 0");

		panel.add(btnNewButton, "cell 0 3");

		txtId = new JTextField();
		txtId.setText("ID");
		panel.add(txtId, "cell 0 4");
		txtId.setColumns(10);

		txtFirstName = new JTextField();
		txtFirstName.setText("First Name");
		panel.add(txtFirstName, "cell 0 5,growy");
		txtFirstName.setColumns(10);

		txtSecondName = new JTextField();
		txtSecondName.setText("Second Name");
		panel.add(txtSecondName, "cell 0 6");
		txtSecondName.setColumns(10);
		panel.add(btnNewButton_2, "cell 0 10");

		txtId_1 = new JTextField();
		txtId_1.setText("ID");
		panel.add(txtId_1, "cell 0 11");
		txtId_1.setColumns(10);

		JButton btnDeletePatient = new JButton("Delete Patient");
		btnDeletePatient.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				try {

					PatientsDAO pa = new PatientsDAO();
					int ID = Integer.parseInt(txtId_1.getText());
					boolean  res = pa.DeletePatient(ID);
					if (res == false)
						msg.setText("Msg: delete failed!");
					else
						msg.setText("Msg: delete successful!");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

		});
		panel.add(btnDeletePatient, "cell 0 12");
		

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 0, 784, 28);
		frmPatient.getContentPane().add(panel_1);
		panel_1.setLayout(new MigLayout("", "[][]", "[]"));

		JLabel lblWelcome = new JLabel("Welcome:");
		panel_1.add(lblWelcome, "cell 0 0");

		JLabel lblDes = new JLabel("Des");
		panel_1.add(lblDes, "cell 1 0");

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(141, 28, 590, 498);
		frmPatient.getContentPane().add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);
		
		msg = new JLabel("Msg:");
		panel.add(msg, "cell 0 14");
	}

	public void showTableData() throws SQLException {

		try {
			String theDbUrl = "jdbc:mysql://localhost:3306/pms";
			Statement stmt = Conn_db.connection(theDbUrl);
			ResultSet rs = stmt.executeQuery("SELECT * FROM  pms.patient");
			table.setModel(DbUtils.resultSetToTableModel(rs));
		}catch(

	ClassNotFoundException e)
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}catch(
	Exception e)
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
}
