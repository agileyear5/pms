package ie.ait.eng.sw.msc.agile.dao; 

  

import java.sql.ResultSet; 

import java.sql.SQLException; 

import java.sql.Statement; 

  

import ie.ait.eng.sw.msc.agile.util.Patients; 

import ie.ait.eng.sw.msc.agile.util.Staff;
import ie.ait.eng.sw.msc.agile.util.pharmacist; 

  

public class PharDAO { 

  

    private Statement stmt = Conn_db.connection(); 

  

    public PharDAO() throws Exception { 

  

    } 

  

    private pharmacist processRow(ResultSet rs) throws SQLException { 

    	pharmacist pharmacist = new pharmacist(); 

    	pharmacist.setID(rs.getInt("ID")); 

    	pharmacist.setfirstname(rs.getString("Firstname")); 

    	pharmacist.setSurname(rs.getString("surname"));

    	pharmacist.setPassword(rs.getString("pass"));

    	pharmacist.setAddress(rs.getString("address"));

    	pharmacist.setTelephone(rs.getString("telephone"));

    	pharmacist.setEmail(rs.getString("email"));

        return pharmacist; 

    } 
    
    
    
    //Search Engine
    public pharmacist searchEngine(String id) {

		pharmacist phr = null;

		try {

			String sql = "SELECT * FROM pms.phar where '" + id + "' in (id,firstname,surname,pass,Address,Telephone,Email)";

			ResultSet resultset = stmt.executeQuery(sql);

			while (resultset.next()) {

				phr = processRow(resultset);

				phr.setID(resultset.getInt("id"));

				phr.setfirstname(resultset.getString("firstname"));

				phr.setSurname(resultset.getString("surname"));

				phr.setPassword(resultset.getString("pass"));

				phr.setAddress(resultset.getString("address"));

				phr.setTelephone(resultset.getString("telephone"));

				phr.setEmail(resultset.getString("email"));

				

			}

		} catch (SQLException ex) {

			System.err.println("SQLException!");

			ex.printStackTrace();

		}

		return phr;

	}
	
  

    public pharmacist search(int id) { 

    	pharmacist pharmacist  = null; 

  

        try { 

  

            String sql = "select * from pms.phar where ID='" + id + "' "; 

            ResultSet resultset = stmt.executeQuery(sql); 

  

            while (resultset.next()) { 

            	pharmacist = processRow(resultset); 

            	pharmacist.setID(resultset.getInt("id")); 

            	pharmacist.setfirstname(resultset.getString("firstname")); 

            	pharmacist.setPassword(resultset.getString("pass")); 

              

            } 

        } catch (SQLException ex) { 

            System.err.println("SQLException!"); 

            ex.printStackTrace(); 

        } 

  

        return pharmacist; 

    } 

  


} 
