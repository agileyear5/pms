
package ie.ait.eng.sw.msc.agile.dao;

	import ie.ait.eng.sw.msc.agile.dao.Conn_db;
	import java.util.*;
	import java.util.logging.Level;
	import java.util.logging.Logger;

	import com.mysql.jdbc.Connection;
	import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
	import java.sql.*;
	import java.io.*;
	import ie.ait.eng.sw.msc.agile.util.Patients;
	import ie.ait.eng.sw.msc.agile.util.Staff;
public class AdminDAO {

	



		private ResultSet rs = null;
	    private Properties prop = new Properties();
	    private InputStream input = null;
	    private Statement stmt = Conn_db.connection();
	    
	    
		public AdminDAO() throws Exception {
			
		}
		public ResultSet rs (String sql) {
			try {
				rs = stmt.executeQuery(sql);
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return rs;
		}
		public List<Staff> findAll() throws ClassNotFoundException {
			List<Staff> list = new ArrayList<>();
			try {
				rs("SELECT * FROM  pms.patient");
				while (rs.next()) {
					
					Staff staff = processRow(rs);
					//int patientId = rs.getInt("ID");
					//String firstName = rs.getString("FIRSTNAME");
					//String secondName = rs.getString("SURNAME");
					//System.out.println(patientId + "\t" + firstName + "\t" + secondName);
					list.add(processRow(rs));
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			return list;
		}

		private Staff processRow(ResultSet rs) throws SQLException {
			Staff staff = new Staff(0, null, null,null,null, null, null, null);
			staff.setId(rs.getInt("id"));
			staff.setFirstName(rs.getString("firstName"));
			staff.setSurname(rs.getString("surname"));
			staff.setPassword(rs.getString("password"));
			staff.setRole(rs.getString("role"));

			return staff;
		}

	
		public Staff findStaff(String name) throws ClassNotFoundException {
		//	List<Patients> list = new ArrayList<>();
			Staff staff=null;
			try {
			
	            rs("SELECT * FROM pms.staff where FIRSTNAME = '" + name + "' ");
				while (rs.next()) {
					staff = processRow(rs);
					staff.setId(rs.getInt("id"));
					staff.setFirstName(rs.getString("firstname"));
					staff.setSurname(rs.getString("surname"));
					staff.setRole(rs.getString("role"));
					
				
				}
			} catch (SQLException ex) {
				System.err.println("SQLException!");
				ex.printStackTrace();
			}
			return staff;
		}

		public Staff findStaff(int id) throws ClassNotFoundException {
			Staff staff=null;
			try {
				Statement stmt = Conn_db.connection();
				rs("SELECT * FROM pms.staff where ID = '" + id + "' ");
				while (rs.next()) {
					staff = processRow(rs);
					staff.setId(rs.getInt("id"));
					staff.setFirstName(rs.getString("firstname"));
					staff.setSurname(rs.getString("surname"));
					staff.setRole(rs.getString("role"));
					
			
				}
			} catch (SQLException ex) {
				System.err.println("SQLException!");
				ex.printStackTrace();
			}
			return staff;
		}


		
		
		public String add(Object object) {

			Staff staff = (Staff) object;
			int Id = staff.getId();
			String FirstName = staff.getFirstName();
			String Surname = staff.getSurname();
			String Password = staff.getSurname();
			String Role = staff.getRole();

			String result = "";
			int rowcount;

			try {
				String query = "Insert into pms.staff (ID,FIRSTNAME,SURNAME,PASSWORD,ROLE) values" + " ('" + Id + "', '"
						+ FirstName + "', '" + Surname + "', '"+Password+"','"+Role+"')";
				rowcount = stmt.executeUpdate(query);
				if (rowcount > 0) {
					
					result = "true";
					System.out.println("staff inserted successful");
				} else {
					result = "false:The data could not be inserted in the databse";
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return result;
		}


		   public boolean update(Staff staff) { 
			   
			   int id = staff.getId();
			   
			   String firstname = staff.getFirstName();
			   
			   String surname = staff.getSurname();

		        try { 

		            String updateSql = "update pms.staff set firstname='" + firstname + "', surname='" + surname 

		                    + "' where ID='" + id + "' "; 	  

		            int result = stmt.executeUpdate(updateSql); 

		            if (result != 0) { 

		                System.out.println("staff Information Updated Successfully......."); 

		            } else { 

		                System.out.println("Nothing Updated ......."); 

		            } 

		        } catch (SQLException ex) { 

		            System.err.println("SQLException!"); 

		            ex.printStackTrace(); 

		        } 
		        return true; 

		    } 
		
		
		      
		public boolean DeleteStaff(int ID) {
	        
			try {
				if (findStaff(ID) != null) {
					try {
						stmt.executeUpdate("DELETE FROM pms.staff where ID = '" + ID + "' ");
						System.out.println("staff ID Deleted " + ID);

					} catch (SQLException ex) {
						System.err.println("SQLException!");
						ex.printStackTrace();
					}
					return true;
				} else {
					System.out.println("the staff were already deleted");
					return false;
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}

		public void DeleteStaff(String Name) {

			try {
				if (findStaff(Name) != null) {
					try {
						stmt.executeUpdate("DELETE FROM pms.staff where FIRSTNAME = '" + Name + "' ");
						System.out.println("staff Name Deleted" + Name);

					} catch (SQLException ex) {
						System.err.println("SQLException!");
						ex.printStackTrace();
					}
				} else {
					System.out.println("the patient were already deleted");
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
