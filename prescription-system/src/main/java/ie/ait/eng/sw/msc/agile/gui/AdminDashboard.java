package ie.ait.eng.sw.msc.agile.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Panel;

import javax.swing.JButton;
import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JProgressBar;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AdminDashboard {
    private JFrame frame;
    private JFrame frmAdmindashboard;

    /**
     * Launch the application.
     */
    
     public static void main(String[] args) { EventQueue.invokeLater(new Runnable() { public void run() { try { AdminDashboard window = new AdminDashboard();
     window.frmAdmindashboard.setVisible(true); } catch (Exception e) { e.printStackTrace(); } } }); }
     

    /**
     * Create the application.
     */
    /**
     * 
     */
    public AdminDashboard() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmAdmindashboard = new JFrame();
        frmAdmindashboard.setVisible(true);
        frmAdmindashboard.setTitle("AdminDashboard");
        frmAdmindashboard.setBounds(100, 100, 800, 600);
        frmAdmindashboard.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmAdmindashboard.getContentPane().setLayout(new BorderLayout(0, 0));

        Panel panel_1 = new Panel();
        frmAdmindashboard.getContentPane().add(panel_1, BorderLayout.SOUTH);

        JButton btnCreateNewGp = new JButton("Create New GP");
        btnCreateNewGp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Register();
			}
		});

        JButton button = new JButton("Create New Phar");
        button.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent arg0) {
        		new CreatePharGUI();
        	}
        });

        JButton btnChangePassword = new JButton("Change Password");

        JProgressBar progressBar = new JProgressBar();

        JButton btnReadAllAccounts = new JButton("Read All Accounts");

        JButton btnUpdate = new JButton("Update an Account");
        btnUpdate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            }
        });

        JButton btnLogOut = new JButton("Log Out");
        btnLogOut.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Login l = new Login();
                l.main(null);
                frmAdmindashboard.setVisible(false);
            }
        });
        GroupLayout gl_panel_1 = new GroupLayout(panel_1);
        gl_panel_1.setHorizontalGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_panel_1.createSequentialGroup().addContainerGap(636, Short.MAX_VALUE).addComponent(progressBar,
                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(gl_panel_1.createSequentialGroup().addContainerGap()
                        .addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING).addComponent(btnUpdate)
                                .addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
                                        .addComponent(btnReadAllAccounts, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnCreateNewGp, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(button, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnChangePassword, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addContainerGap(599, Short.MAX_VALUE))
                .addGroup(Alignment.TRAILING, gl_panel_1.createSequentialGroup().addContainerGap().addComponent(btnLogOut)));
        gl_panel_1.setVerticalGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
                .addGroup(gl_panel_1.createSequentialGroup().addContainerGap().addComponent(btnLogOut).addGap(34).addComponent(btnCreateNewGp)
                        .addPreferredGap(ComponentPlacement.UNRELATED).addComponent(button).addPreferredGap(ComponentPlacement.UNRELATED)
                        .addComponent(btnChangePassword).addPreferredGap(ComponentPlacement.UNRELATED).addComponent(btnReadAllAccounts)
                        .addPreferredGap(ComponentPlacement.UNRELATED).addComponent(btnUpdate)
                        .addPreferredGap(ComponentPlacement.RELATED, 255, Short.MAX_VALUE)
                        .addComponent(progressBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
        panel_1.setLayout(gl_panel_1);

        JLabel lblWelcomeToGp = new JLabel("   Welcome to GP System!");
        frmAdmindashboard.getContentPane().add(lblWelcomeToGp, BorderLayout.NORTH);
    }
}
