
package ie.ait.eng.sw.msc.agile.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public enum Role {

    GP, PHARMACIST, PATIENT, OTHER;
    public Connection connectDB() throws SQLException {

		Connection con = null;
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/pms";
			con = DriverManager.getConnection(url, "admin", "admin");
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n"
					+ e.getMessage());
		}
		return con;
	}
	public boolean registerToDB(String fname1,String password1) {
		System.out.println(fname1);
		
		System.out.println(password1);
		
		
		try {
			Connection con = connectDB();
			PreparedStatement pst = (PreparedStatement)con.prepareStatement("INSERT INTO GP (FIRSTNAME,PASS)  VALUES (?, ?)");
			System.out.println(pst.toString());
			pst.setString(1, fname1);
			pst.setString(2, password1);
			
		
			int rowsInserted = pst.executeUpdate();
			if (rowsInserted > 0)
				return true;
			else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean createPharToDB(String fname1,String sname1,String password1,String address1,String telepnone1,String email1) {
		
		System.out.println(fname1);
		System.out.println(sname1);
		System.out.println(password1);
		System.out.println(address1);
		System.out.println(telepnone1);
		System.out.println(email1);
		
		
		try {
			Connection con = connectDB();
			PreparedStatement pst = (PreparedStatement)con.prepareStatement("INSERT INTO PHAR (FIRSTNAME,SURNAME,PASS,ADDRESS,TELEPHONE,EMAIL)  VALUES (?, ?, ?, ?, ?, ?)");
			System.out.println(pst.toString());
			pst.setString(1, fname1);
			pst.setString(2, sname1);
			pst.setString(3, password1);
			pst.setString(4, address1);
			pst.setString(5, telepnone1);
			pst.setString(6, email1);
		
			int rowsInserted = pst.executeUpdate();
			if (rowsInserted > 0)
				return true;
			else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
