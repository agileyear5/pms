package ie.ait.eng.sw.msc.agile.util;

public class Staff {
 private int id;
 private String firstName,surname,password,address,telephone,email,role;

 
    public Staff() {
	super();
}


	public Staff(int id, String firstName, String surname, String password,String address,String telephone,String email, String role) {
	super();
	this.id = id;
	this.firstName = firstName;
	this.surname = surname;
	this.password = password;
	this.address = address;
	this.telephone = telephone;
	this.email = email;
	this.role = role;
}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getTelephone() {
		return telephone;
	}


	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}


	@Override
	public String toString() {
		return "Staff [id=" + id + ", firstName=" + firstName + ", surname=" + surname + ", password=" + password
				+ ",address=" + address + ", telephone=" + telephone + ", email=" + email + ",  role=" + role + "]";
	}
	
    
}
