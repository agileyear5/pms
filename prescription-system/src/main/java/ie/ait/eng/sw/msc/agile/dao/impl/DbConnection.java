package ie.ait.eng.sw.msc.agile.dao.impl;

import java.sql.*;
import com.mysql.jdbc.Driver;

public class DbConnection {

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/PMS";

    //  Database credentials
    static final String USER = "admin";
    static final String PASS = "admin";

    public static Connection getConnection() {
        try {
            Connection conn;
            Class.forName("com.mysql.jdbc.Driver");

            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            return conn;
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
            return null;
        }
    }

}