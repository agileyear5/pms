package ie.ait.eng.sw.msc.agile.dao;

public class ExceptionHandlerClass extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    String message;

    public ExceptionHandlerClass(String errMessage) {
        message = errMessage;
    }

    public String getMessage() {
        return message;
    }
}
