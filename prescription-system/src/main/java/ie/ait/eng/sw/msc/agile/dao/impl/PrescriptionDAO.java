package ie.ait.eng.sw.msc.agile.dao; 

  

import java.sql.ResultSet; 

import java.sql.SQLException; 

import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ie.ait.eng.sw.msc.agile.util.Prescription;
import ie.ait.eng.sw.msc.agile.util.Staff; 

  

public class PrescriptionDAO { 

  

    private Statement stmt = Conn_db.connection(); 

  

    public PrescriptionDAO() throws Exception { 

  

    } 

  

    private Prescription processRow(ResultSet rs) throws SQLException { 

    	Prescription prescription = new Prescription(); 

    	prescription.setId(rs.getInt("ID")); 

    	prescription.setPatientId(rs.getInt("patientid")); 

    	prescription.setPrescription(rs.getString("prescription")); 

    	prescription.setAllergies(rs.getString("ALLERGIES")); 

    	prescription.setAdvice_info(rs.getString("ADVICE_INFO")); 
    	
    	prescription.setComments(rs.getString("comments")); 

  

        return prescription; 

    } 

   /// Search Engine
    public Prescription SearchEngine(String id) { 

    	Prescription prescription = null; 

  

        try { 

  

            String sql = "SELECT * FROM pms.prescriptions where '" + id + "' in (ID,patientid,prescription,ALLERGIES,ADVICE_INFO,comments)"; 

            ResultSet rs = stmt.executeQuery(sql); 

  

            while (rs.next()) { 

            	prescription = processRow(rs); 
            	
            	prescription.setId(rs.getInt("ID")); 

            	prescription.setPatientId(rs.getInt("patientid")); 

            	prescription.setPrescription(rs.getString("prescription")); 

            	prescription.setAllergies(rs.getString("ALLERGIES")); 

            	prescription.setAdvice_info(rs.getString("ADVICE_INFO")); 
            	
            	prescription.setComments(rs.getString("comments")); 

                 

            } 

        } catch (SQLException ex) { 

            System.err.println("SQLException!"); 

            ex.printStackTrace(); 

        } 

  

        return prescription; 

    } 


    public Prescription search(int patientId) { 

    	Prescription prescription = null; 

  

        try { 

  

            String sql = "select * from pms.prescriptions where PATIENTID='" + patientId + "' "; 

            ResultSet rs = stmt.executeQuery(sql); 

  

            while (rs.next()) { 

            	prescription = processRow(rs); 
            	
            	prescription.setId(rs.getInt("ID")); 

            	prescription.setPatientId(rs.getInt("patientid")); 

            	prescription.setPrescription(rs.getString("prescription")); 

            	prescription.setAllergies(rs.getString("ALLERGIES")); 

            	prescription.setAdvice_info(rs.getString("ADVICE_INFO")); 
            	
            	prescription.setComments(rs.getString("comments")); 

                 

            } 

        } catch (SQLException ex) { 

            System.err.println("SQLException!"); 

            ex.printStackTrace(); 

        } 

  

        return prescription; 

    } 

  
    public List<Prescription> getAll() throws SQLException {
    	List<Prescription> list = new ArrayList<>();
    	String sql = "select * from pms.prescriptions"; 
        ResultSet rs = stmt.executeQuery(sql); 
        while(rs.next()) {
        	Prescription pres = new Prescription();
        	//pres = processRow(rs);
        	pres.setId(rs.getInt("ID"));
        	pres.setPatientId(rs.getInt("PATIENTID"));
        	pres.setPrescription(rs.getString("PRESCRIPTION"));
        	pres.setAllergies("ALLERGIES");
        	pres.setAdvice_info(rs.getString("ADVICE_INFO"));
        	pres.setComments(rs.getString("COMMENTS"));
        	list.add(pres);	
        }
    	return list; 
    	
    }
    public boolean updatePres(Prescription p) throws SQLException {
    	String sql = "update pms.prescriptions set PATIENTID='"+p.getPatientId()+
    			"' PRESCRIPTION='"+p.getPrescription()+"' ALLERGIES='"+p.getAllergies()+
    			"' ADVICE_INFO='"+p.getAdvice_info()+"' COMMENTS='"+p.getComments()+"' where ID='" + p.getId() + "' "; 

        ResultSet rs = stmt.executeQuery(sql); 
        if(rs.next())
        	return true;
        else
        	return false;

    }
    
    public boolean deletePres(int id) throws SQLException {
    	String sql = "delete * from pms.prescriptions where ID='" + id + "' "; 

        ResultSet rs = stmt.executeQuery(sql); 
        if(rs.next())
        	return true;
        else
        	return false;

    }

    public boolean update(int id, Staff newStaff) { 

  

        String firstname = newStaff.getFirstName(); 

        String surname = newStaff.getSurname(); 

        String password = newStaff.getPassword(); 

        String role = newStaff.getRole(); 

  

        try { 

  

            String updateSql = "update pms.staff set firstname='" + firstname + "', surname='" + surname 

                    + "', password='" + password + "', role='" + role + "' where ID='" + id + "' "; 

  

            int result = stmt.executeUpdate(updateSql); 

  

            if (result != 0) { 

                System.out.println("Staff Information Updated Successfully......."); 

            } else { 

                System.out.println("Nothing Updated ......."); 

            } 

  

        } catch (SQLException ex) { 

            System.err.println("SQLException!"); 

            ex.printStackTrace(); 

        } 

  

        return true; 

    } 

  

    public static void main(String[] args) { 

  

    } 

  

} 