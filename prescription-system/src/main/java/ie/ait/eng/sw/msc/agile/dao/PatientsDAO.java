package ie.ait.eng.sw.msc.agile.dao;

import ie.ait.eng.sw.msc.agile.dao.Conn_db;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.*;
import java.io.*;
import ie.ait.eng.sw.msc.agile.util.Patients;
import ie.ait.eng.sw.msc.agile.util.Staff;

public class PatientsDAO {

	private ResultSet rs = null;
    private Properties prop = new Properties();
    private InputStream input = null;
    private Statement stmt = Conn_db.connection();
    
    
	public PatientsDAO() throws Exception {
		
	}
	public ResultSet rs (String sql) {
		try {
			rs = stmt.executeQuery(sql);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return rs;
	}
	public List<Patients> findAll() throws ClassNotFoundException {
		List<Patients> list = new ArrayList<>();
		try {
			rs("SELECT * FROM  pms.patient");
			while (rs.next()) {
				
				Patients patients = processRow(rs);
				list.add(processRow(rs));
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return list;
	}

	private Patients processRow(ResultSet rs) throws SQLException {
		Patients Patient = new Patients(0, null, null, null, null, null, null);
		Patient.setId(rs.getInt("ID"));
		Patient.setFirstName(rs.getString("FirstName"));
		Patient.setSecondName(rs.getString("SurName"));
		Patient.setGender(rs.getString("gender"));
		Patient.setAddress(rs.getString("Address"));
		Patient.setTelephone(rs.getString("Telephone"));
		Patient.setEmail(rs.getString("Email"));

		return Patient;
	}

	public Patients findPatient(String name) throws ClassNotFoundException {
		Patients patient=null;
		try {
		
            rs("SELECT * FROM pms.patient where FIRSTNAME = '" + name + "' ");
			while (rs.next()) {
				patient = processRow(rs);
			}
		} catch (SQLException ex) {
			System.err.println("SQLException!");
			ex.printStackTrace();
		}
		return patient;
	}
	
	public Patients findPatient(int id) throws ClassNotFoundException {
		Patients patient=null;
		try {
			Statement stmt = Conn_db.connection();
			rs("SELECT * FROM pms.patient where ID = '" + id + "' ");
			while (rs.next()) {
				patient = processRow(rs);
				patient.setId(rs.getInt("ID"));
				patient.setFirstName(rs.getString("firstname"));
				patient.setSecondName(rs.getString("surname"));
				patient.setGender(rs.getString("gender"));
				patient.setAddress(rs.getString("Address"));
				patient.setTelephone(rs.getString("Telephone"));
				patient.setEmail(rs.getString("Email"));
			}
		} catch (SQLException ex) {
			System.err.println("SQLException!");
			ex.printStackTrace();
		}
		return patient;
	}


	public Patients SearchEngine(String id) throws ClassNotFoundException {
		Patients patient=null;
		try {
			Statement stmt = Conn_db.connection();
			rs("SELECT * FROM pms.patient where '" + id + "' in (ID,firstname,surname,gender,Address,Telephone,Email)");
			while (rs.next()) {
				patient = processRow(rs);
				patient.setId(rs.getInt("ID"));
				patient.setFirstName(rs.getString("firstname"));
				patient.setSecondName(rs.getString("surname"));
				patient.setGender(rs.getString("gender"));
				patient.setAddress(rs.getString("Address"));
				patient.setTelephone(rs.getString("Telephone"));
				patient.setEmail(rs.getString("Email"));
			}
		} catch (SQLException ex) {
			System.err.println("SQLException!");
			ex.printStackTrace();
		}
		return patient;
	}
	
	
	
	public String add(Object object) {
		Patients patients = (Patients) object;
		int Id = patients.getId();
		String FirstName = patients.getFirstName();
		String SecondName = patients.getSecondName();
		String gender= patients.getGender();
		String address= patients.getAddress();
		String telephone= patients.getTelephone();
		String email= patients.getEmail();
		String result = "";
		int rowcount;

		try {
			String query = "Insert into pms.patient (ID,FIRSTNAME,SURNAME,Gender,Address,Telephone,email) values" + " ('" + Id + "', '"
					+ FirstName + "', '" + SecondName + "' ,'" + gender + "','" + address + "','" + telephone + "','"
					+ email + "')";
			rowcount = stmt.executeUpdate(query);
			if (rowcount > 0) {
				
				result = "true";
				System.out.println("Patient inserted successful");
			} else {
				result = "false:The data could not be inserted in the databse";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}


	   public boolean update(Patients newPatients) { 
		   
		   int id = newPatients.getId();
		   String firstname = newPatients.getFirstName();
		   String surname = newPatients.getSecondName();
		   String gender= newPatients.getGender();
		   String address= newPatients.getAddress();
		   String telephone= newPatients.getTelephone();
		   String email= newPatients.getEmail();

	        try { 

	          
	            
	            String updateSql = "update pms.patient set firstname='" + firstname + "', surname='" + surname
						+ "', gender='" + gender + "', address='" + address + "',telephone='" + telephone + "',email='"
						+ email + "' where ID='" + id + "' ";

	            int result = stmt.executeUpdate(updateSql); 

	            if (result != 0) { 

	                System.out.println("patient Information Updated Successfully......."); 

	            } else { 

	                System.out.println("Nothing Updated ......."); 

	            } 

	        } catch (SQLException ex) { 

	            System.err.println("SQLException!"); 

	            ex.printStackTrace(); 

	        } 
	        return true; 

	    } 
	
	
	      
	public boolean DeletePatient(int ID) {
        
		try {
			if (findPatient(ID) != null) {
				try {
					stmt.executeUpdate("DELETE FROM pms.patient where ID = '" + ID + "' ");
					System.out.println("patient ID Deleted " + ID);

				} catch (SQLException ex) {
					System.err.println("SQLException!");
					ex.printStackTrace();
				}
				return true;
			} else {
				System.out.println("the patient were already deleted");
				return false;
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public void DeletePatient(String Name) {

		try {
			if (findPatient(Name) != null) {
				try {
					stmt.executeUpdate("DELETE FROM pms.patient where FIRSTNAME = '" + Name + "' ");
					System.out.println("patient Name Deleted" + Name);

				} catch (SQLException ex) {
					System.err.println("SQLException!");
					ex.printStackTrace();
				}
			} else {
				System.out.println("the patient were already deleted");
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
