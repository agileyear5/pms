package ie.ait.eng.sw.msc.agile.dao;

import java.sql.ResultSet;

import java.sql.SQLException;

import java.sql.Statement;

import ie.ait.eng.sw.msc.agile.util.Staff;

public class StaffDAO {

	private Statement stmt = Conn_db.connection();

	public StaffDAO() throws Exception {

	}

	private Staff processRow(ResultSet rs) throws SQLException {

		Staff staff = new Staff();

		staff.setId(rs.getInt("ID"));

		staff.setFirstName(rs.getString("FirstName"));

		staff.setSurname(rs.getString("SurName"));

		staff.setPassword(rs.getString("Password"));

		staff.setPassword(rs.getString("Address"));

		staff.setPassword(rs.getString("Telephone"));

		staff.setPassword(rs.getString("Email"));

		staff.setRole(rs.getString("Role"));

		return staff;

	}

	public Staff search(int id) {

		Staff staff = null;

		try {

			String sql = "select * from pms.staff where ID='" + id + "' ";

			ResultSet resultset = stmt.executeQuery(sql);

			while (resultset.next()) {

				staff = processRow(resultset);

				staff.setId(resultset.getInt("id"));

				staff.setFirstName(resultset.getString("firstname"));

				staff.setSurname(resultset.getString("surname"));

				staff.setPassword(resultset.getString("password"));

				staff.setAddress(resultset.getString("address"));

				staff.setTelephone(resultset.getString("telephone"));

				staff.setEmail(resultset.getString("email"));

				staff.setRole(resultset.getString("role"));

			}

		} catch (SQLException ex) {

			System.err.println("SQLException!");

			ex.printStackTrace();

		}

		return staff;

	}
	
	
	//Search Engine
	public Staff searchEngine(String id) {

		Staff staff = null;

		try {

			String sql = "SELECT * FROM pms.staff where '" + id + "' in (id,firstname,surname,password,Address,Telephone,Email,role)";

			ResultSet resultset = stmt.executeQuery(sql);

			while (resultset.next()) {

				staff = processRow(resultset);

				staff.setId(resultset.getInt("id"));

				staff.setFirstName(resultset.getString("firstname"));

				staff.setSurname(resultset.getString("surname"));

				staff.setPassword(resultset.getString("password"));

				staff.setAddress(resultset.getString("address"));

				staff.setTelephone(resultset.getString("telephone"));

				staff.setEmail(resultset.getString("email"));

				staff.setRole(resultset.getString("role"));

			}

		} catch (SQLException ex) {

			System.err.println("SQLException!");

			ex.printStackTrace();

		}

		return staff;

	}
	
	public boolean update(Staff newStaff) {

		int id = newStaff.getId();

		String firstname = newStaff.getFirstName();

		String surname = newStaff.getSurname();

		String password = newStaff.getPassword();

		String address = newStaff.getAddress();

		String telephone = newStaff.getTelephone();

		String email = newStaff.getEmail();

		String role = newStaff.getRole();

		try {

			String updateSql = "update pms.staff set firstname='" + firstname + "', surname='" + surname
					+ "', password='" + password + "', address='" + address + "',telephone='" + telephone + "',email='"
					+ email + "', role='" + role + "' where ID='" + id + "' ";

			int result = stmt.executeUpdate(updateSql);

			if (result != 0) {

				System.out.println("Update staff information successfully!");

			} else {
			}

		} catch (SQLException ex) {

			System.err.println("SQLException!");

			ex.printStackTrace();

		}

		return true;

	}

	public static void main(String[] args) {

	}

}