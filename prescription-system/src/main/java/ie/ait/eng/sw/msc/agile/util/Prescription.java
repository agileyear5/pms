package ie.ait.eng.sw.msc.agile.util;

public class Prescription {
	private int id, PatientId;
	private String prescription,allergies,advice_info,comments;

	public Prescription() {
		super();
	}
	
	public Prescription(int id, int patientid, String prescription, String allergies, String advice_info, String comments) {
		super();
		this.id = id;
		PatientId = patientid;
		this.prescription = prescription;
		this.allergies = allergies;
		this.advice_info = advice_info;
		this.comments = comments;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPatientId() {
		return PatientId;
	}

	public void setPatientId(int patientId) {
		PatientId = patientId;
	}

	public String getPrescription() {
		return prescription;
	}

	public void setPrescription(String prescription) {
		this.prescription = prescription;
	}

	public String getAllergies() {
		return allergies;
	}

	public void setAllergies(String allergies) {
		this.allergies = allergies;
	}

	public String getAdvice_info() {
		return advice_info;
	}

	public void setAdvice_info(String advice_info) {
		this.advice_info = advice_info;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	@Override
	public String toString() {
		return "Prescription [id=" + id + ", PatientId=" + PatientId + ", prescription=" + prescription + ", allergies="
				+ allergies + ", advice_info=" + advice_info + ",comments=" + comments + "]";
	}
	
	
	
}