package ie.ait.eng.sw.msc.agile.util;

public class pharmacist {
	private int id;
	private String firstname,surname,password,address,telephone,email;
	
public pharmacist() {
}

public pharmacist(int ID, String firstname,String surname,String password,String address,String telephone,String email) {
this.id = ID;
this.firstname=firstname;
this.surname=surname;
this.password = password;
this.address=address;
this.telephone=telephone;
this.email=email;
}


public int getID() {
return id;
}

public void setID(int ID) {
this.id = ID;
}


public String getfirstname() {
return firstname;
}

public void setfirstname(String firstname) {
this.firstname = firstname;
}

public String getSurname() {
return surname;
}

public void setSurname(String surname) {
this.surname = surname;
}

public String getPassword() {
return password;
}

public void setPassword(String password) {
this.password = password;
}

public void setAddress(String address) {
this.address = address;
}

public String getAddress() {
return address;
}

public void setTelephone(String telephone) {
this.surname = telephone;
}

public String getTelephone() {
return telephone;
}

public void setEmail(String email) {
this.email = email;
}

public String getEmail() {
return email;
}

public boolean CreatePharGUI()
{
	
	boolean result=Role.PHARMACIST.createPharToDB(firstname,surname,password,address,telephone,email);
	return result;
}


}


