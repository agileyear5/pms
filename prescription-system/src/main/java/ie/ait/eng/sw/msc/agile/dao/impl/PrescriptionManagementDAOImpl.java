package ie.ait.eng.sw.msc.agile.dao.impl;

import ie.ait.eng.sw.msc.agile.dao.impl.DbConnection;

import java.sql.*;

import ie.ait.eng.sw.msc.agile.dao.PrescriptionManagementDAO;
import ie.ait.eng.sw.msc.agile.util.Role;
import ie.ait.eng.sw.msc.agile.util.Prescription;


public class PrescriptionManagementDAOImpl implements PrescriptionManagementDAO {
	Connection connection;
    PreparedStatement ps;
    ResultSet rs;


	public PrescriptionManagementDAOImpl() {
		connection = DbConnection.getConnection();
		if (connection == null) {
			System.exit(1);
		}
	}

	public boolean isDbConnected() {
		try {
			return !connection.isClosed();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean login(String userName, String password) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "SELECT * FROM STAFF where FIRSTNAME=? AND PASSWORD =?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, userName);
			ps.setString(2, password);
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			System.err.println(e);
			return false;
		} finally {
			ps.close();
			rs.close();
		}

	}

	public String validateLogin(String userName, String password) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "SELECT * FROM STAFF where FIRSTNAME=? AND PASSWORD =?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, userName);
			ps.setString(2, password);
			rs = ps.executeQuery();
			if (!rs.next()) {
				return null;
			} else {
				return rs.getString(5);
			}

		} catch (SQLException e) {
			System.err.println(e);
			return null;
		} finally {
			ps.close();
			rs.close();
		}

	}

	public boolean PatientLogin(String firstname, String surname) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "SELECT * FROM Patient where FIRSTNAME=? AND SURNAME=? ";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, firstname);
			ps.setString(2, surname);
			rs = ps.executeQuery();
			if (rs.next())
				return true;
			else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

	}

	public int getID(String firstname, String surname) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "SELECT id FROM Patient where FIRSTNAME=? AND SURNAME=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, firstname);
			ps.setString(2, surname);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt("id");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		return 0;

	}

	public boolean StaffLogin(String firstname, String surname, String password, String role) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "SELECT * FROM STAFF where FIRSTNAME=? AND SURNAME=? AND PASSWORD =? AND ROLE =?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, firstname);
			ps.setString(2, surname);
			ps.setString(3, password);
			ps.setString(4, role);
			rs = ps.executeQuery();
			if (rs.next())
				return true;
			else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

	}

	public String getAllPatient() {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "SELECT * FROM STAFF";
		try {
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ie.ait.eng.sw.msc.agile.dao.PrescriptionManagementDAO#logout()
	 */
	@Override
	public boolean logout() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ie.ait.eng.sw.msc.agile.dao.PrescriptionManagementDAO#createAccount(java.
	 * lang.String, java.lang.String, java.lang.Enum)
	 */
	@Override
	public boolean createAccount(String userName, String password, Enum<Role> role) {
		// TODO Auto-generated method stub
		return false;
	}
	
    public Prescription getPrescription(int pid) throws SQLException {

    	Prescription p = new Prescription();
    	String selectQuery = "SELECT * FROM PRESCRIPTIONS where ID=?";
    	try {
    		ps = connection.prepareStatement(selectQuery);
    		ps.setInt(1, pid);
    		rs = ps.executeQuery();
    		
    		while(rs.next()) {
    			int prescriptionId = rs.getInt(1);
    			int patientId = rs.getInt(2);
    			String prescription = rs.getString(3);
    			String allergies = rs.getString(4);
    			String adviceInfo = rs.getString(5);
    			String comments = rs.getString(6);
    			
    			System.out.println("PATIENT ID: " + patientId);
    			p.setId(prescriptionId);
    			p.setPatientId(patientId);
    			p.setPrescription(prescription);
    			p.setAllergies(allergies);
    			p.setAdvice_info(adviceInfo);
    			p.setComments(comments);
    		}
		} catch (Exception e) {
			System.err.println(e);
		} finally {
            ps.close();
            rs.close();			
		}
		return p;
    }

    public int updatePrescription(Prescription p) throws SQLException {

    	int resultCode = 0;
    	String updateQuery = "UPDATE PRESCRIPTIONS SET PRESCRIPTION=?, ALLERGIES=?, ADVICE_INFO=?, COMMENTS=? WHERE ID=?";
    	try {
    		ps = connection.prepareStatement(updateQuery);
            ps.setString(1, p.getPrescription());
            ps.setString(2, p.getAllergies());
            ps.setString(3, p.getAdvice_info());
            ps.setString(4, p.getComments());
            ps.setInt(5, p.getId());
            
            resultCode = ps.executeUpdate();
            System.out.println("Updating prescription: " + p.getId());
            System.out.println("New values : " + p.getPrescription() + ", " + p.getAllergies() + ", " + p.getAdvice_info() + ", " + p.getComments());
            System.out.println("Result code is " + resultCode);
    		
		} catch (Exception e) {
			System.err.println(e);
			resultCode = -1;
		} finally {
            ps.close();			
		}
		return resultCode;
    }

    public String getPatientName(int id) throws SQLException {

    	String fullName = "";
    	String selectQuery = "SELECT FIRSTNAME, SURNAME FROM PATIENT where ID=?";
    	try {
    		ps = connection.prepareStatement(selectQuery);
    		ps.setInt(1, id);
    		rs = ps.executeQuery();
    		
    		while(rs.next()) {
    			String firstName = rs.getString(1);
    			String surname = rs.getString(2);
    			
    			fullName = firstName + " " + surname;
    			System.out.println("FIRSTNAME: " + firstName);
    			System.out.println("SURNAME: " + surname);
    		}
		} catch (Exception e) {
			System.err.println(e);
		} finally {
            ps.close();
            rs.close();			
		}
		return fullName;
    }
}
