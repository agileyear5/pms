package ie.ait.eng.sw.msc.agile.dao;

import java.sql.SQLException;

import ie.ait.eng.sw.msc.agile.util.Role;

public interface PrescriptionManagementDAO {
    boolean login(String userName, String password) throws SQLException;

    boolean logout();

    boolean createAccount(String userName, String password, Enum<Role> role);
}