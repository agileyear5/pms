package ie.ait.eng.sw.msc.agile.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class CreatePages {

    private JFrame frame;
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    CreatePages window = new CreatePages();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public CreatePages() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 450, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JLabel lblUserName = new JLabel("User Name:");
        lblUserName.setBounds(103, 59, 80, 18);
        frame.getContentPane().add(lblUserName);

        textField = new JTextField();
        textField.setBounds(240, 56, 86, 24);
        frame.getContentPane().add(textField);
        textField.setColumns(10);

        JLabel lblPassword = new JLabel("Password:");
        lblPassword.setBounds(103, 91, 72, 18);
        frame.getContentPane().add(lblPassword);

        textField_1 = new JTextField();
        textField_1.setBounds(240, 88, 86, 24);
        frame.getContentPane().add(textField_1);
        textField_1.setColumns(10);

        JLabel lblConfirmPassword = new JLabel("Confirm Password:");
        lblConfirmPassword.setBounds(103, 122, 136, 18);
        frame.getContentPane().add(lblConfirmPassword);

        textField_2 = new JTextField();
        textField_2.setBounds(240, 119, 86, 24);
        frame.getContentPane().add(textField_2);
        textField_2.setColumns(10);

        JButton btnReset = new JButton("Reset");
        btnReset.setBounds(136, 173, 80, 27);
        frame.getContentPane().add(btnReset);

        JButton btnCreate = new JButton("Create");
        btnCreate.setBounds(230, 173, 81, 27);
        frame.getContentPane().add(btnCreate);
    }
}
