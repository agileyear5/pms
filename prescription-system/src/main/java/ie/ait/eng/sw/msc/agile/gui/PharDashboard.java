package ie.ait.eng.sw.msc.agile.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.Panel;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

import ie.ait.eng.sw.msc.agile.dao.PatientsDAO;

import javax.swing.JButton;
import javax.swing.BoxLayout;
import net.miginfocom.swing.MigLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PharDashboard {

    private JFrame frmPhardashboard;

    /**
     * Launch the application.
     */
    
      public static void main(String[] args) { EventQueue.invokeLater(new Runnable() { public void run() { try { PharDashboard window = new PharDashboard(); window.frmPhardashboard.setVisible(true);
      } catch (Exception e) { e.printStackTrace(); } } }); }
     

    /**
     * Create the application.
     */
    public PharDashboard() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmPhardashboard = new JFrame();
        frmPhardashboard.setTitle("PharDashboard");
        frmPhardashboard.setBounds(100, 100, 800, 600);
        frmPhardashboard.setVisible(true);
        frmPhardashboard.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Panel panel = new Panel();
        frmPhardashboard.getContentPane().add(panel, BorderLayout.NORTH);
        panel.setLayout(new MigLayout("", "[64px][48px][89px][][][][][][][][][][][][][][][][][]", "[27px]"));

        JLabel lblWelcome = new JLabel("Welcome:");
        panel.add(lblWelcome, "cell 0 0,alignx left,aligny center");

        JLabel lblNewLabel = new JLabel("Mr Des");
        panel.add(lblNewLabel, "cell 1 0,alignx left,aligny center");
                
                        JButton btnLogOut = new JButton("Log out");
                        btnLogOut.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                Login l = new Login();
                                l.main(null);
                                frmPhardashboard.setVisible(false);
                            }
                        });
                        panel.add(btnLogOut, "cell 19 0,alignx left,aligny top");

        Panel panel_1 = new Panel();
        frmPhardashboard.getContentPane().add(panel_1, BorderLayout.WEST);
        panel_1.setLayout(new MigLayout("", "[185px]", "[27px][][][][][][][][][][][][]"));

        JButton btnCreatePrescription = new JButton("Create Prescription");
        panel_1.add(btnCreatePrescription, "cell 0 0,alignx left,aligny top");
                                
                                        JButton btnViewPrescription = new JButton("View Prescription");
                                        panel_1.add(btnViewPrescription, "cell 0 2,alignx left,aligny top");
                                
                                        JButton btnEditPrescription = new JButton("Edit Prescription");
                                        panel_1.add(btnEditPrescription, "cell 0 4,alignx left,aligny top");
                                        
                                        JButton btnFindPatient = new JButton("Find One Patient BY Name");
                                        btnFindPatient.addMouseListener(new MouseAdapter() {
                                        	@Override
                                        	public void mouseClicked(MouseEvent e) {
                                        		
                                        		
												try {
													PatientsDAO pa = new PatientsDAO();
													pa.findPatient("name");
													System.out.println(""+pa.findPatient("name"));
													
												} catch (Exception e1) {
													// TODO Auto-generated catch block
													e1.printStackTrace();
												}
                            					
                                        	}
                                        });
                                        btnFindPatient.addActionListener(new ActionListener() {
                                        	public void actionPerformed(ActionEvent e) {
                                        	}
                                        });
                                        panel_1.add(btnFindPatient, "cell 0 6");
                                        
                                        JButton btnFindOnePatient = new JButton("Find One Patient BY ID");
                                        btnFindOnePatient.addMouseListener(new MouseAdapter() {
                                        	@Override
                                        	public void mouseClicked(MouseEvent e) {
                                        		try {
                                        			PatientsDAO pa = new PatientsDAO();
													System.out.println(""+pa.findPatient(50));
													
												} catch (Exception e1) {
													// TODO Auto-generated catch block
													e1.printStackTrace();
												}
                                        		
                                        	}
                                        });
                                        panel_1.add(btnFindOnePatient, "cell 0 7");
                                        
                                        JButton btnGetAllPatients = new JButton("Get All Patients");
                                        btnGetAllPatients.addMouseListener(new MouseAdapter() {
                                        	@Override
                                        	public void mouseClicked(MouseEvent e) {
                                        		try {
                                        			PatientsDAO pa =new PatientsDAO();                                                       		
                               			      System.out.println("  the patents: " + pa.findAll());                               	                                				
                                					} catch (Exception e1) {
                                					// TODO Auto-generated catch block
                                					e1.printStackTrace();
                                				}
                                        	}
                                        });
                                        panel_1.add(btnGetAllPatients, "cell 0 9");
                                        
                                        JButton btnDeleteAPatient = new JButton("Delete A Patient Dy Name");
                                        btnDeleteAPatient.addMouseListener(new MouseAdapter() {
                                        	@Override
                                        	public void mouseClicked(MouseEvent e) {
                                        		try {
                                        			PatientsDAO pa = new PatientsDAO();
                                	        		pa.DeletePatient("Name");
                                				} catch (Exception e1) {
                                					// TODO Auto-generated catch block
                                					e1.printStackTrace();
                                				}
                                        	}
                                        });
                                        panel_1.add(btnDeleteAPatient, "cell 0 11");
                                        
                                        JButton btnDeleteAPatient_1 = new JButton("Delete A Patient Dy ID");
                                        btnDeleteAPatient_1.addMouseListener(new MouseAdapter() {
                                        	
                                        	@Override
                                        	public void mouseClicked(MouseEvent e) {
                                        		try {
                                        			PatientsDAO pa = new PatientsDAO();
                                	        		pa.DeletePatient(50);
                                				} catch (Exception e1) {
                                					// TODO Auto-generated catch block
                                					e1.printStackTrace();
                                				}
                                        	}
                                        });
                                        panel_1.add(btnDeleteAPatient_1, "cell 0 12");
    }

}
