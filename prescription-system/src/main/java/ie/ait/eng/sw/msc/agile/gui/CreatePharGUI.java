package ie.ait.eng.sw.msc.agile.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import ie.ait.eng.sw.msc.agile.util.pharmacist;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class CreatePharGUI {

	private JFrame frmCreatePhar;
	private JTextField textField;
	private JTextField textField_2;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
/*	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreatePharGUI window = new CreatePharGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
*/
	/**
	 * Create the application.
	 */
	public CreatePharGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCreatePhar = new JFrame();
		frmCreatePhar.setTitle("Create Phar");
		frmCreatePhar.setVisible(true);
		frmCreatePhar.setBounds(100, 100, 450, 300);
		frmCreatePhar.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCreatePhar.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("FirstName");
		lblNewLabel.setBounds(28, 56, 71, 20);
		frmCreatePhar.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("Password");
		lblNewLabel_2.setBounds(28, 156, 67, 20);
		frmCreatePhar.getContentPane().add(lblNewLabel_2);
		
		textField = new JTextField();
		textField.setBounds(114, 56, 86, 20);
		frmCreatePhar.getContentPane().add(textField);
		textField.setColumns(10);
		

		passwordField = new JPasswordField();
		passwordField.setBounds(114, 156, 86, 20);
		frmCreatePhar.getContentPane().add(passwordField);
		
		/*textField_2 = new JTextField();
		textField_2.setBounds(114, 156, 86, 20);
		frmCreatePhar.getContentPane().add(textField_2);
		textField_2.setColumns(10);*/
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(textField.getText().length()==0)  
				      JOptionPane.showMessageDialog(null, "Firstname Required");
				  
				   else if(passwordField.getText().length()==0)  
					      JOptionPane.showMessageDialog(null, "Password Required");
				 
				   else{
					   
				String fname1 = textField.getText();
			//	String sname1 = textField_1.getText(); 
				String password1 = passwordField.getText(); 
			//	String type1 = textField_3.getText(); 
			
				
				pharmacist temp=new pharmacist(0, fname1, password1, password1, password1, password1, password1);
				boolean result=temp.CreatePharGUI();
				if(result)
				{
					JOptionPane.showMessageDialog(null, "You are now Registered on our System!");
					new AdminDashboard();
					
				}
				
				frmCreatePhar.dispose();
				//Register(fname1,lname1,email1,password1,type_id);
			}}
		});
		btnRegister.setBounds(290, 209, 89, 23);
		frmCreatePhar.getContentPane().add(btnRegister);
		
	}
}
