package ie.ait.eng.sw.msc.agile.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import ie.ait.eng.sw.msc.agile.util.GP;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class Register {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_2;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Register window = new Register();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the application.
	 */
	public Register() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("FirstName");
		lblNewLabel.setBounds(28, 56, 46, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("Password");
		lblNewLabel_2.setBounds(28, 156, 46, 14);
		frame.getContentPane().add(lblNewLabel_2);
		
		textField = new JTextField();
		textField.setBounds(111, 50, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(111, 153, 86, 20);
		frame.getContentPane().add(passwordField);
		
		/*textField_2 = new JTextField();
		textField_2.setBounds(111, 153, 86, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);*/
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(textField.getText().length()==0)  
				      JOptionPane.showMessageDialog(null, "Firstname Required");
				  
				   else if(passwordField.getText().length()==0)  
					      JOptionPane.showMessageDialog(null, "Password Required");
				 
				   else{
					   
			//	Integer id=Integer.valueOf(textField_4.getText());
				String fname1 = textField.getText();
			//	String sname1 = textField_1.getText(); 
				String password1 = passwordField.getText(); 
			//	String type1 = textField_3.getText(); 
			
				
				GP temp=new GP(fname1, password1);
				boolean result=temp.register();
				if(result)
				{
					JOptionPane.showMessageDialog(null, "You are now Registered on our System!");
					new AdminDashboard();
					
				}
				//Register(fname1,lname1,email1,password1,type_id);
			}}
			
		});
		btnRegister.setBounds(290, 209, 89, 23);
		frame.getContentPane().add(btnRegister);
		
		
	}
}
