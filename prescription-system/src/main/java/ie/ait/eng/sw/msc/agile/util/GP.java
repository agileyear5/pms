package ie.ait.eng.sw.msc.agile.util;




import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class GP {
	private int userId;
	private String fname;
	private String surname;
	private String role;
	private String password;

	
	
	public GP(String fn, String p) {
		//userId=id;
		fname = fn;
		//surname = sn;
	//	role = r;
		password = p;
	}
	public void setName(String name)
	{
		this.fname=name;
	}
	
	public void setId(int id)
	{
		this.userId=id;
	}
	public void setSurname(String surname)
	{
		this.surname=surname;
	}

	public void setPassword(String password)
	{
		this.password=password;
	}
	public void setRole(String role)
	{
		this.role=role;
	}
	

	public boolean register()
	{
		
		boolean result=Role.GP.registerToDB(fname, password);
		return result;
	}
	
}

