package ie.ait.eng.sw.msc.agile.gui;

import java.awt.EventQueue;
import java.awt.List;
import java.awt.*;
import javax.swing.JFrame;
import java.awt.Panel;
import java.awt.BorderLayout;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.sql.*;

import ie.ait.eng.sw.msc.agile.dao.PatientsDAO;
import ie.ait.eng.sw.msc.agile.dao.impl.DbConnection;
import ie.ait.eng.sw.msc.agile.util.Patients;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTable;

public class GPDashboard {
 
    private JFrame frmGpdashboard; 
    
     
    /**
     * Launch the application.
     */ 
    
      public static void main(String[] args) { EventQueue.invokeLater(new Runnable() { public void run() { try { GPDashboard window = new GPDashboard(); window.frmGpdashboard.setVisible(true); }
      catch (Exception e) { e.printStackTrace(); } } }); }
     

    /**
     * Create the application.
     */
    public GPDashboard() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmGpdashboard = new JFrame();
        frmGpdashboard.setVisible(true);
        frmGpdashboard.setTitle("GPDashboard");
        frmGpdashboard.setBounds(100, 100, 800, 600);
        frmGpdashboard.setVisible(true);
        frmGpdashboard.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Panel panel = new Panel();
        frmGpdashboard.getContentPane().add(panel, BorderLayout.NORTH);
        panel.setLayout(new MigLayout("", "[][][][][][][][][][][][][][][][][][][][][][][][][]", "[]"));

        JLabel lblWelcome = new JLabel("Welcome:");
        panel.add(lblWelcome, "cell 0 0");

        JLabel lblDes = new JLabel("Des");
        panel.add(lblDes, "cell 1 0");

        JButton btnLogOut = new JButton("Log out");
        panel.add(btnLogOut, "cell 24 0");
        
        JPanel panel_1 = new JPanel();
        frmGpdashboard.getContentPane().add(panel_1, BorderLayout.WEST);
        panel_1.setLayout(new MigLayout("", "[]", "[][][][][][][][][]"));
        
        JButton btnCreateNewPatien = new JButton("Create New Patient");
        btnCreateNewPatien.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent arg0) {
        		
        		try {
        			PatientsDashboard  pd = new PatientsDashboard();
        			
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        	}
        });
        panel_1.add(btnCreateNewPatien, "cell 0 0");
        
        JButton btnFin = new JButton("Find One Patient by FirstName");
        btnFin.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        		try {
					PatientsDAO pa = new PatientsDAO();
					pa.findPatient("name");
					System.out.println(""+pa.findPatient("name"));
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
        		
        	}
        });
        btnFin.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	}
        });
        panel_1.add(btnFin, "cell 0 3");
        
        JButton btnFindOne = new JButton("Find One Patient By ID");
        btnFindOne.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		
				try {
					PatientsDAO pa= new PatientsDAO();
					pa.findPatient(50);
					System.out.println(""+pa.findPatient(50));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        	}
        });
        panel_1.add(btnFindOne, "cell 0 4");
        
        JButton btnGatAllPatients = new JButton("Gat All Patients");
        btnGatAllPatients.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		try {
					PatientsDAO pa =new PatientsDAO();
				
					//List<Patients> PatientsList = pa.findAll();
				//	for (Patients Patients : PatientsList) {
			      System.out.println("  the patents: " + pa.findAll());
					//  JTable.(pa.findAll());
				
					} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
        	}
        		
        });
        panel_1.add(btnGatAllPatients, "cell 0 5");
        
        JButton btnNewButton = new JButton("Delete A Patient BY ID");
        btnNewButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
				try {
					PatientsDAO pa = new PatientsDAO();
	        		pa.DeletePatient(50);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
        
        	}
        });
        panel_1.add(btnNewButton, "cell 0 6");
        
        JButton btnDeleteAPatient = new JButton("Delete A Patient BY Name");
        btnDeleteAPatient.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent arg0) {
        	
				try {
					PatientsDAO pa = new PatientsDAO();
	        		pa.DeletePatient("Name");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	
        	}
        });
        panel_1.add(btnDeleteAPatient, "cell 0 7");
        

     

    }}
