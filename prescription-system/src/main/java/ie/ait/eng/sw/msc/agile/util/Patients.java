package ie.ait.eng.sw.msc.agile.util;

import javax.xml.bind.annotation.XmlElement;//JAXB
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

public class Patients {
	private int id;
	private String FirstName, SecondName;
	private String Gender;
	private String Address;
	private String Telephone;
	private String Email;
	public Patients() {

	}


	public Patients(int id, String firstName, String secondName,String gender,String address,String telephone,String email) {
		this.id = id;
		FirstName = firstName;
		SecondName = secondName;
		Gender=gender;
		Address=address;
		Telephone=telephone;
		Email=email;
		
	}
	public Patients(String firstName, String secondName,String gender,String address,String telephone,String email) {
		FirstName = firstName;
		SecondName = secondName;
		Gender=gender;
		Address=address;
		Telephone=telephone;
		Email=email;
		
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}
	public String getTelephone() {
		return Telephone;
	}

	public void setTelephone(String telephone) {
		Telephone = telephone;
	}
	
	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}


	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getSecondName() {
		return SecondName;
	}

	public void setSecondName(String secondName) {
		SecondName = secondName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Patients [id=" + id + ", firstName=" + FirstName + ", SecondName=" + SecondName + ", gender=" + Gender + ", address="
				+ Address + ",telephone=" + Telephone + ", email=" + Email + "]";
		
	}

}
