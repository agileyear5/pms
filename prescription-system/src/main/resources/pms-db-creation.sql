adminCREATE DATABASE IF NOT EXISTS PMS;
use PMS;

DROP TABLE IF EXISTS PRESCRIPTIONS;
DROP TABLE IF EXISTS PATIENT;
DROP TABLE IF EXISTS STAFF;
DROP TABLE IF EXISTS GP;
DROP TABLE IF EXISTS Phar;
DROP TABLE IF EXISTS ADMIN;
DROP TABLE IF EXISTS PRESCRIPTIONS;


CREATE TABLE STAFF (
 ID        	INT		NOT NULL AUTO_INCREMENT,
 FIRSTNAME	CHAR(20) NOT NULL, 
 SURNAME	CHAR(20) NOT NULL,
 PASSWORD	CHAR(10) NOT NULL,
 ADDRESS	CHAR(40),
 TELEPHONE	CHAR(15),
 EMAIL		CHAR(40),
 ROLE		CHAR(20) NOT NULL,
 PRIMARY KEY (ID)
);
 
INSERT INTO STAFF VALUES (1,'Des','Howlin', 'agile2017', 'Dublin Road, Athlone', '0906495431', 'des.howlin@student.ait.ie', 'ADMIN');
INSERT INTO STAFF VALUES (2, 'Wei','Hu', 'agile2017', 'Monksland, Athlone', '0876298274', 'wei.hu@student.ait.ie', 'GP');
INSERT INTO STAFF VALUES (3, 'Xu','Luo', 'agile2017', 'Merrion Sq., Dublin', '018623512', 'xu.luo@student.ait.ie', 'PHARMACIST');
INSERT INTO STAFF VALUES (4, 'Hui','Yuan', 'agile2017', 'Roscommon town', '065852647', 'hui.yuan@student.ait.ie', 'OTHER');

CREATE TABLE ADMIN(
ID        	INT		NOT NULL AUTO_INCREMENT,
FIRSTNAME      CHAR(20) NOT NULL,
PASS  CHAR(10) NOT NULL,
PRIMARY KEY (ID)
);

INSERT INTO ADMIN VALUES (1,'Des','Howlin');

CREATE TABLE GP(
ID        	INT		NOT NULL AUTO_INCREMENT,
FIRSTNAME      CHAR(20) NOT NULL,
SURNAME	CHAR(20) NOT NULL,
PASS  CHAR(10) NOT NULL,
ADDRESS	CHAR(40),
TELEPHONE	CHAR(15),
EMAIL		CHAR(30),
PRIMARY KEY (ID)
);

INSERT INTO GP VALUES (1, 'Wei','Hu', 'hu', 'Monksland, Athlone', '0876298274', 'wei.hu@student.ait.ie');
INSERT INTO GP VALUES (2, 'Osama','Alghamdi', 'agile2017', 'Dublin Road, Athlone', '0876298274', 'osama.alghamdi@student.ait.ie');


CREATE TABLE Phar(
ID        	INT		NOT NULL AUTO_INCREMENT,
FIRSTNAME      CHAR(20) NOT NULL, 
SURNAME	CHAR(20) NOT NULL,
PASS  CHAR(10) NOT NULL,
ADDRESS	CHAR(40),
TELEPHONE	CHAR(15),
EMAIL		CHAR(30),
PRIMARY KEY (ID)
);

INSERT INTO Phar VALUES (1, 'xu','luo', 'luo', 'Woodville, Athlone', '0876298274', 'xu.luo@student.ait.ie');
INSERT INTO Phar VALUES (2, 'ding','cheng', 'agile2017', 'Main St., Ballinsloe', '0874398651', 'ding.cheng@student.ait.ie');


CREATE TABLE PATIENT (
 ID			INT NOT NULL AUTO_INCREMENT,
 FIRSTNAME	CHAR(20) NOT NULL,
 SURNAME	CHAR(20) NOT NULL,
 GENDER		CHAR(1),
 ADDRESS	CHAR(40),
 TELEPHONE	CHAR(15),
 EMAIL		CHAR(30),
 PRIMARY KEY (ID)
 );

INSERT INTO PATIENT VALUES (1, 'Ossama','Alghamdi', 'M', 'Woodville, Athlone', '0876298274', 'xu.luo@student.ait.ie');
INSERT INTO PATIENT VALUES (2, 'Dingcheng','Lu', 'M', 'Main St., Ballinsloe', '0874398651', 'ding.cheng@student.ait.ie');
INSERT INTO PATIENT VALUES (3, 'Li','Liu', 'F', 'Roscommon town', '065852647', 'hui.yuan@student.ait.ie');
 
CREATE TABLE PRESCRIPTIONS (
 ID				INT NOT NULL AUTO_INCREMENT,
 PATIENTID		INT NOT NULL,
 PRESCRIPTION	CHAR(40) NOT NULL, 
 ALLERGIES		CHAR(40), 
 ADVICE_INFO	CHAR(40),
 COMMENTS		CHAR(50),
 PRIMARY KEY (ID),
 FOREIGN KEY (PATIENTID) REFERENCES PATIENT(ID) ON DELETE CASCADE
 );
 
INSERT INTO PRESCRIPTIONS VALUES (1, 3, 'Painkillers', 'take 2 a day', NULL, NULL);
INSERT INTO PRESCRIPTIONS VALUES (2, 2, 'Steroids', 'unknown', 'take after meals', NULL); 
INSERT INTO PRESCRIPTIONS VALUES (3, 1, 'Sinutab', 'none', 'take in the morning', 'may cause drowsiness');