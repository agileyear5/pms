package ie.ait.eng.sw.msc.agile.dao.impl;

import static org.junit.Assert.*;


import org.junit.BeforeClass;
import org.junit.Test;

import ie.ait.eng.sw.msc.agile.dao.StaffDAO;

import ie.ait.eng.sw.msc.agile.util.Staff;
import junit.framework.Assert;

public class StaffDAOTest {
	private static StaffDAO staffDAO;
	private Staff staff = new Staff();
	private int testID = 3;

	@BeforeClass
	public static void setup() throws Exception {
		staffDAO = new StaffDAO();
	}

	@Test
	public void testSearch() {
		Staff staff = staffDAO.search(testID);
		Assert.assertEquals(testID, staff.getId());
		Assert.assertEquals("Xu", staff.getFirstName());
		Assert.assertEquals("Luo", staff.getSurname());
		Assert.assertEquals("agile2018", staff.getPassword());
		Assert.assertEquals("PHARMACIST", staff.getRole());
		return;
	}

	@Test
	public void testUpdate() {
		// Staff staff = new Staff();
		staff.setId(testID);
		staff.setFirstName("Xu");
		staff.setSurname("Luo");
		staff.setPassword("agile2018");
		staff.setRole("PHARMACIST");

		staffDAO.update(staff);
		int id = staff.getId();
		Assert.assertNotNull(id);

		Staff updatedstaff = staffDAO.search(id);

		Assert.assertEquals("Xu", updatedstaff.getFirstName());
		Assert.assertEquals("Luo", updatedstaff.getSurname());
		Assert.assertEquals("agile2018", updatedstaff.getPassword());
		Assert.assertEquals("PHARMACIST", updatedstaff.getRole());

	}

}
