package ie.ait.eng.sw.msc.agile.dao.impl;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.*;

public class PrescriptionManagementDAOImplTest {

    private static PrescriptionManagementDAOImpl dao;

    @BeforeClass
    public static void setup() {
        dao = new PrescriptionManagementDAOImpl();
    }

    @Test
    public void testDbConnectionIsSuccessful() {
        assertTrue(dao.isDbConnected());
    }

    @Test
    public void testUserLoginIsSuccessful() throws SQLException {
        assertTrue(dao.login("Des", "agile2017"));
    }

    @Test
    public void testUserLoginIsUnsuccessful() throws SQLException {
        assertFalse(dao.login("Des", "agile"));
    }

    @Test
    public void testValidateLoginIsSuccessful() throws SQLException {
    	String expectedRole = "ADMIN"; 
    	String actualRole = dao.validateLogin("Des", "agile2017");
        assertTrue(actualRole.equals(expectedRole));
    }

}
