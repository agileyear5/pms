package ie.ait.eng.sw.msc.agile.dao.impl;

import org.junit.Before;
import org.junit.Test;

import ie.ait.eng.sw.msc.agile.util.Role;
import ie.ait.eng.sw.msc.agile.util.pharmacist;
import junit.framework.Assert;

public class pharmacistTest {
	private static pharmacist phar;
	
	@Before
	public void testBefore(){
		
		phar=new pharmacist();
	}

	@Test
	public void testSetAndGetMethod() throws ClassNotFoundException{
		phar.setfirstname("xu");
		phar.setPassword("123");
		Assert.assertEquals("xu", phar.getfirstname());
		Assert.assertEquals("123", phar.getPassword());
	}
	@Test
	public void testCreatePharGUI() throws ClassNotFoundException{
		phar.setfirstname("xu");
		phar.setPassword("123");
		phar.CreatePharGUI();
		Assert.assertTrue(Role.PHARMACIST.createPharToDB("xu","123"));
		
	}
	


}
