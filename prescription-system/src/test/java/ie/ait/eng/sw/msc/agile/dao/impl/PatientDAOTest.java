package ie.ait.eng.sw.msc.agile.dao.impl;

import static junit.framework.Assert.assertFalse;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.List;
import java.util.Properties;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.Statement;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import ie.ait.eng.sw.msc.agile.dao.Conn_db;
import ie.ait.eng.sw.msc.agile.dao.ExceptionHandlerClass;
import ie.ait.eng.sw.msc.agile.dao.PatientsDAO;
import ie.ait.eng.sw.msc.agile.util.Patients;
import junit.framework.Assert;

public class PatientDAOTest {


	private static PatientsDAO dao;
	private static Conn_db con;
	private Patients patients = new Patients();
	private int testID=259;
    private Properties prop = new Properties();
    private InputStream input = null;
    
	@BeforeClass
	public static void setup() throws Exception {
		dao = new PatientsDAO();
	}

	@Test
	public void testDbConnectionIsSuccessful() throws ExceptionHandlerClass, IOException {

		
        
        input = new FileInputStream("config.properties"); 
        prop.load(input);
		Assert.assertTrue(Conn_db.getConn(prop.getProperty("database")));


	}
	
	 @Test
	    public void testFindPatient() throws ClassNotFoundException
	    {
		 Patients newPatient = (Patients) dao.findPatient(testID);

		    Assert.assertEquals(testID , newPatient.getId());
			Assert.assertEquals("Osama",newPatient.getFirstName());
			Assert.assertEquals("Alghamdi",newPatient.getSecondName());
	
			
		 
	    }

	@Test
	public void AddPatient()  {

		
		patients.setId(testID);
		patients.setFirstName("Osama");
		patients.setSecondName("Alghamdi");
		
		try {
			dao.add(patients);
			
			int id = patients.getId();
			System.out.println("id:"+id);
			Patients newPatient = (Patients) dao.findPatient(id);
			Assert.assertEquals(testID, newPatient.getId());
			Assert.assertEquals("Osama", newPatient.getFirstName());
			Assert.assertEquals("Alghamdi", newPatient.getSecondName());
		
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	
		
		return;
		
	}

	@Ignore
	@Test
	public void testUserLoginIsUnsuccessful() throws SQLException, ClassNotFoundException {

		// Assert.assertEquals(dao.findPatient(ID);
	}

}
