package ie.ait.eng.sw.msc.agile.dao.impl;

import static org.junit.Assert.*;

import org.junit.Test;

import ie.ait.eng.sw.msc.agile.dao.Conn_db;

public class GPandAdminLoginTestTest {


	@Test
	
	// Test number: 1
				// Test objective: Test invalid input
				// Input(s): username="li",password=""
				// Expected Output(s): Boolean = false
				// Added by: li
				public void test001() {
				 Conn_db testObject = new Conn_db();
					try {
						assertFalse(testObject.compareWithSql("li", ""));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
	@Test
				// Test number: 2
				// Test objective: Test invalid input
				// Input(s): username="",password="123"
				// Expected Output(s): Boolean = false
				// Added by: li
				public void test002() {
					 Conn_db testObject = new Conn_db();
					try {
						assertFalse(testObject.compareWithSql("", "123"));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
	@Test
				// Test number: 3
				// Test objective: Test invalid input
				// Input(s): username="",password=""
				// Expected Output(s): Boolean = false
				// Added by: li
				public void test003() {
					 Conn_db testObject = new Conn_db();
					try {
						assertFalse(testObject.compareWithSql("", ""));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
	@Test
				// Test number: 4
				// Test objective: Test valid input
				// Input(s): username="admin",password="pass"
				// Expected Output(s): Boolean = false
				// Added by: li
				public void test004() {
					 Conn_db testObject = new Conn_db();
					try {
						assertTrue(testObject.compareWithSql("admin", "pass"));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
	@Test
				// Test number: 5
				// Test objective: Test invalid input
				// Input(s): username="admin",password="pass123"
				// Expected Output(s): Boolean = false
				// Added by: li
				public void test005() {
					 Conn_db testObject = new Conn_db();
					try {
						assertFalse(testObject.compareWithSql("admin", "pass123"));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
	@Test
				// Test number: 6
				// Test objective: Test invalid input
				// Input(s): username="admin",password=""
				// Expected Output(s): Boolean = false
				// Added by: li
				public void test006() {
					 Conn_db testObject = new Conn_db();
					try {
						assertFalse(testObject.compareWithSql("admin", ""));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
	@Test
				// Test number: 7
				// Test objective: Test invalid input
				// Input(s): username="",password="pass"
				// Expected Output(s): Boolean = false
				// Added by: li
				public void test007() {
					 Conn_db testObject = new Conn_db();
					try {
						assertFalse(testObject.compareWithSql("", "pass"));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
	@Test
				// Test number: 8
				// Test objective: Test invalid input
				// Input(s): username="adminnn",password="pass"
				// Expected Output(s): Boolean = false
				// Added by: li
				public void test008() {
					 Conn_db testObject = new Conn_db();
					try {
						assertFalse(testObject.compareWithSql("adminnn", "pass"));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
}
