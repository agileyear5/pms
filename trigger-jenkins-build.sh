#!/bin/sh

JENKINS_SERVER="http://localhost:8080/jenkins"
JOB="PMS"
USER="autobuild"
PASSWORD="build"
TOKEN="Msc2017"
REPO_URL="https://A00237906@bitbucket.org/agileyear5/pms.git"

# trigger a build
# curl --user autobuild:build -s http://localhost:8080/jenkins/job/Agile-Demo/build?token=Msc2017 <--this works
# or 
# curl --user autobuild:build -s http://localhost:8080/jenkins/git/notifyCommit?url=https://A00237906@bitbucket.org/A00237906/agiledemo.git <--not working
echo "Triggering a build of ${REPO_URL}"
curl --user ${USER}:${PASSWORD} -s ${JENKINS_SERVER}/job/${JOB}/build?token=${TOKEN}

echo "Waiting 20secs for build to execute before checking the status"
# this should poll rather than wait, but test with sleep to get working, 
# poll in a while loop would be better
sleep 20
_status_of_build_=$(curl --user ${USER}:${PASSWORD} -s ${JENKINS_SERVER}/job/${JOB}/lastBuild/api/json | sed -e 's/[{}]/''/g' | awk -v k="text" '{n=split($0,a,","); for (i=1; i<=n; i++) print a[i]}' | grep result | awk -F":" '{print $2}')

_build_number_=$(curl --user ${USER}:${PASSWORD} -s ${JENKINS_SERVER}/job/${JOB}/lastBuild/api/json | sed -e 's/[{}]/''/g' | awk -v k="text" '{n=split($0,a,","); for (i=1; i<=n; i++) print a[i]}' | grep buildNumber | awk -F":" '{print $2}')
echo "The result of build number ${_build_number_} was ${_status_of_build_}"